package Fakes;

import game.GameManager;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;

import display.DisplayManager;

public class FakeDisplay extends DisplayManager implements Runnable {

	long lastFrame;
	int fps;
	long lastFPS;

	protected void initGL() {

	}

	@Override
	public void start(GameManager gm, int hSize, int vSize, int frameLimit) {
		initialize(gm);
		this.lastFPS = getTime();
		long nowTime = System.currentTimeMillis() * 1000L * FakePlay.TIME_SCALE;
		long nextTime = System.currentTimeMillis() * 1000L
				* FakePlay.TIME_SCALE;
		int numUpdates = 0;

		while ((!Display.isCloseRequested()) && (!gm.isExitFlag())) {

			// System.out.println("--------- Update ----------");
			update(gm);
			updateTime();
			updateFPS();
			++numUpdates;

			nowTime = System.currentTimeMillis() * 1000L * FakePlay.TIME_SCALE;
			long RestTime = nextTime - nowTime;
			if (RestTime > 0L) {
				try {
					Thread.sleep(RestTime / (1000L * FakePlay.TIME_SCALE));
				} catch (Exception localException) {
				}
			} else {
				try {
					Thread.sleep(0L);
				} catch (Exception localException1) {
				}
			}
			nextTime += 16666L;

		}

		System.out.println("Llamadas a update -> " + numUpdates);
		if (!gm.isExitFlag()) {
			gm.exit();
		}
		Sincro.Inst.RemoveGame();
		// System.out.println("End fake Display");
	}

	private void initialize(GameManager gm) {
		gm.initialize();
	}

	private void update(GameManager gm) {
		gm.update();
	}

	private void updateTime() {
		this.lastFrame = getTime();
	}

	private long getTime() {
		// return Sys.getTime() * 1000L * FakePlay.TIME_SCALE
		// / Sys.getTimerResolution();
		return System.currentTimeMillis() * FakePlay.TIME_SCALE;
	}

	private void updateFPS() {
		if (getTime() - this.lastFPS > 1000L) {
			this.fps = 0;
			this.lastFPS += 1000L;
		}
		this.fps += 1;
	}

	GameManager _gm;
	int _hSize;
	int _vSize;
	int _frameLimit;
	int _offset; // Quitar

	public void SetParams(GameManager gm, int hSize, int vSize, int frameLimit,
			int offset) {
		_gm = gm;
		_hSize = hSize;
		_vSize = vSize;
		_frameLimit = frameLimit;
		_offset = offset;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		start(_gm, _hSize, _vSize, _frameLimit);
	}

}
