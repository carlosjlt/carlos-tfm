package Fakes;

import game.GameManager;
import game.GameState;
import game.GraphicManager;
import game.SoundManager;
import gamestate.Launcher;
import gamestate.Menu;
import image.ImageContainer;

import java.io.File;
import java.io.PrintStream;

import SingleManagers.FakeGraphicManager;
import SingleManagers.SingleImageContainer;
import SingleManagers.SingleSoundManager;
import core.Input;

public class FakeGame extends GameManager {
	private static final String[] CHARACTER = { "ZEN", "GARNET", "LUD", "KFM" };
	int[] deviceType;
	String[] aiName;
	String[] characterName;
	boolean automationFlag = false;
	int number = 1;
	boolean allCombinationFlag = false;
	ImageContainer ic;

	public FakeGame(String[] args) {
		String[] aiName = new String[2];
		String[] characterName = new String[2];
		this.number = 1;
		this.automationFlag = true;
		for (int i = 0; i < args.length; i++) {
			if ("--a1".equals(args[i])) {
				aiName[0] = new String(args[(++i)]);
			} else if ("--a2".equals(args[i])) {
				aiName[1] = new String(args[(++i)]);
			} else if ("--c1".equals(args[i])) {
				String str = new String(args[(++i)]);
				for (int j = 0; j < CHARACTER.length; j++) {
					if (CHARACTER[j].equals(str)) {
						characterName[0] = CHARACTER[j];
					}
				}
			} else if ("--c2".equals(args[i])) {
				String str = new String(args[(++i)]);
				for (int j = 0; j < CHARACTER.length; j++) {
					if (CHARACTER[j].equals(str)) {
						characterName[1] = CHARACTER[j];
					}
				}
			} else {
				System.err.println("arguments error: unknown format is exist.");
			}
		}

		this.aiName = aiName;
		this.characterName = characterName;

		this.deviceType = new int[2];
		for (int i = 0; i < this.deviceType.length; i++) {
			this.deviceType[i] = 2;
		}

	}

	public void initialize() {
		this.graphicManager = FakeGraphicManager.GetGraphicManager();
		this.inputManager = new Input();
		this.soundManager = SingleSoundManager.GetSoundManager();
		this.ic = SingleImageContainer.GetImageContainer();

		FakePlay fPlay = new FakePlay();
		fPlay.setting(this.deviceType, this.aiName, this.characterName);
		fPlay.setImageContainer(this.ic);
		int startId = registerGameState(fPlay);
		startGame(startId);
	}

	public void close() {
		super.close();
		this.inputManager.close();
		this.inputManager = null;
	}

}
