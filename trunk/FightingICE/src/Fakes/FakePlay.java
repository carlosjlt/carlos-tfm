package Fakes;
import fighting.Fighting;
import game.GameState;
import game.GraphicManager;
import game.InputManager;
import game.SoundManager;
import image.ImageContainer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import structs.FrameData;
import structs.GameData;
import structs.KeyData;
import core.Input;

public class FakePlay extends GameState {

	public static final int TIME_SCALE = 20;
	
	public static final int STAGE_SIZE_X = 960;
	public static final int STAGE_SIZE_Y = 640;
	public static final String BACK_GROUND_IMAGE = "./data/graphics/BackGround.png.id";
	private int[] deviceType;
	private String[] aiName;
	private String[] characterName;
	private Fighting fighting;
	private ImageContainer ic;
	private boolean resultFlag = false;
	int timeCount;

	public void setImageContainer(ImageContainer ic) {
		this.ic = ic;
	}

	public void setting(int[] deviceType, String[] aiName,
			String[] characterName) {
		this.deviceType = deviceType;
		this.aiName = aiName;
		this.characterName = characterName;
		/*
		for (int i = 0; i < this.deviceType.length; i++) {
			System.out.print(this.deviceType[i] + ",");
		}
		System.out.println();
		if (aiName != null) {
			for (int i = 0; i < this.aiName.length; i++) {
				System.out.print(this.aiName[i] + ",");
			}
			System.out.println();
		}
		for (int i = 0; i < this.characterName.length; i++) {
			System.out.print(this.characterName[i] + ",");
		}
		System.out.println();
		*/
	}

	@Override
	public void close(GraphicManager gm, SoundManager sm, InputManager<?> im) {
//		this.fighting.close();
		this.fighting = null;
		((Input) im).close();
	}

	@Override
	public void initialize(GraphicManager gm, SoundManager sm,
			InputManager<?> im) {
		this.fighting = new Fighting(this.characterName);

		GameData gd = this.fighting.initialize(960, 640,
		/* "./data/graphics/BackGround.png.id" */"", sm, this.deviceType,
				this.aiName, this.ic);
		((Input) im).initialize(this.deviceType, this.aiName);
		this.fighting.initRound(System.currentTimeMillis() * TIME_SCALE);
		((Input) im).startAI(gd);
		Sincro.Inst.IsDone();
	}

	@Override
	public void update(GraphicManager gm, SoundManager sm, InputManager<?> im) {

		if (!this.resultFlag) {
			long nowTime = System.currentTimeMillis() * TIME_SCALE;
			if (!this.fighting.isRoundEnd(nowTime)) {
				KeyData kd = new KeyData((KeyData) im.getData());

				this.fighting.processingFight(kd);

				FrameData fd = this.fighting.getFrameData(nowTime, kd);

				((Input) im).setFrameData(fd);

//				this.fighting.outputLog(kd);

				// this.fighting.drawImage(gm, nowTime);
			} else {
				this.fighting.setRoundScore();
				if (this.fighting.isGameEnd()) {
					
					LogResults() ;
					
					this.resultFlag = true;
					this.exit();
				}
				this.fighting.initRound(nowTime);
			}
		} else {
			toInstant();
			transition(getParentId());
		}
	}

	private void LogResults() {
		File file;
		file = new File("./log/point/"
				+ Thread.currentThread().getName() + ".PLOG");
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter(
					new FileWriter(file)));
			for (int i = 0; i < 3; i++) {
				pw.println(i + "," + fighting.getRoundScore(true, i) + ","
						+ fighting.getRoundScore(false, i));

			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
