
import FuzzyIA.RuleFilesManager;
import core.Display;
import core.Game;

public class ArgsRunMain {
	public static void main(String[] args) throws Exception {
		
		for(String ar : args)
			System.out.println(ar);
		
		RuleFilesManager.InitInstance();
		RuleFilesManager.GetInstance().Reset();
		if (args.length > 0) {
			RuleFilesManager.GetInstance().SetInitFile(
					Integer.parseInt(args[0]), true);
			Game game = new Game(new String[] { "-n", "1", "--a1", "FuzzyGA",
					"--a2", args[1], "--c1", args[2], "--c2", args[3] });
			Display display = new Display();
			display.start(game, 960, 640, 60);
		}
	}
}
