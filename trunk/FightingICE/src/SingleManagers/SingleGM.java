package SingleManagers;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class SingleGM {

	private static SingleGM inst;

	public static void InitInstance()
	{
		if (SingleGM.inst == null)
			SingleGM.inst = new SingleGM();
	}
	
	public static SingleGM GetSingleGM() {
		if (SingleGM.inst == null)
			SingleGM.inst = new SingleGM();

		return SingleGM.inst;
	}

	private SingleGM() {
		int hSize = 960, vSize = 640;

		try {
			System.out.println("Set Display Mode:" + hSize + "*" + vSize);
			Display.setDisplayMode(new DisplayMode(hSize, vSize));
			System.out.println("Create Display");
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println("Initialize OpenGL");

		GL11.glMatrixMode(5889);
		GL11.glLoadIdentity();

		GL11.glMatrixMode(5888);
		GL11.glOrtho(0.0D, 960.0D, 640.0D, 0.0D, 1.0D, -1.0D);

		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);

		org.lwjgl.opengl.Display.setTitle("FightingICE");

		FakeGraphicManager.InitInstance();
	}
	
	public static void EndInstance()
	{
	    Display.destroy();
	    inst = null;
	}
}
