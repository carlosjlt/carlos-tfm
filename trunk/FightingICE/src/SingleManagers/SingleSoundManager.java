package SingleManagers;

import game.SoundManager;

public class SingleSoundManager {
	private static FakeSoundManager sm;

	public static FakeSoundManager GetSoundManager() {
		if (sm == null)
		{
			sm = new FakeSoundManager();
			sm.initialize();
		}

		return sm;
	}

}
