package SingleManagers;

import game.SoundManager;

import java.security.Key;

public class FakeSoundManager extends SoundManager{
	
	public void close(boolean doIt)
	{
		if(doIt)
			super.close();
	}
	
	public void close()
	{
	}
	
	public int loadCipheredResource(String filePath, boolean loop, Key skey)
	{
		try
		{
			super.loadCipheredResource(filePath, loop, skey);
		}
		catch(Exception ex)
		{
			
		}
		return 0;
	}
	
	public int loadResource(String filePath, boolean loop)
	{
		try
		{
			super.loadResource(filePath, loop);
		}
		catch(Exception ex)
		{
			
		}
		return 0;
	}

}
