package SingleManagers;
import render.Image;
import render.ImageTask;
import game.GraphicManager;
import game.RenderTask;


public class FakeGraphicManager extends GraphicManager {

	
	private static FakeGraphicManager inst;
	public static FakeGraphicManager GetGraphicManager()
	{
		if(inst == null)
			inst = new FakeGraphicManager ();
		
		return inst;
	}
	
	private FakeGraphicManager()
	{
		
	}
	
	public static void InitInstance()
	{
		if(inst == null)
			inst = new FakeGraphicManager ();
	}
	

	  public void render()
	  {
	  }
	  
	  protected void pushTask(RenderTask newTask)
	  {
	    this.task.add(newTask);
	  }
	  
	  public void drawImage(Image p, int x, int y, boolean direction)
	  {
	  }
	  
	  public void drawImage(Image p, int x, int y, int sizeX, int sizeY, boolean direction)
	  {
	  }
	  
	  public void drawString(String string, int x, int y)
	  {
	  }
	  
	  public void drawQuad(int x, int y, int sizeX, int sizeY, double red, double green, double blue)
	  {
	  }
	  
	  public void drawLineQuad(int x, int y, int sizeX, int sizeY, double red, double green, double blue)
	  {
	  }
}
