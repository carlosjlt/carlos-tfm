package SingleManagers;
import image.ImageContainer;


public class SingleImageContainer {
	
	private static ImageContainer ic;
	
	public static ImageContainer GetImageContainer ()
	{
		if(ic == null)
			ic = new ImageContainer();
		
		return ic;
	}

	public static void InitInstance() {
		if(ic == null)
			ic = new ImageContainer();
	}

}
