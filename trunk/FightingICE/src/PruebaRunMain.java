import Fakes.FakeDisplay;
import Fakes.FakeGame;
import FuzzyIA.RuleFilesManager;
import SingleManagers.SingleGM;
import SingleManagers.SingleImageContainer;
import SingleManagers.SingleSoundManager;
import core.Display;
import core.Game;

public class PruebaRunMain {

	public static void main(String[] args) throws Exception {

		SingleGM.InitInstance();
		RuleFilesManager.InitInstance();
		SingleImageContainer.InitInstance();

		FakeGame game = new FakeGame(new String[] { "-n", "1", "--a1",
				"FuzzyGA", "--a2", "T3c", "--c1", "ZEN", "--c2", "LUD" });

		FakeDisplay display = new FakeDisplay();
		display.SetParams(game, 960, 640, 60, 0); // display.start(game, 960, 640,
		display.run();

		System.out.println("Fin!!!");
		
		SingleImageContainer.GetImageContainer().finalize();
		SingleSoundManager.GetSoundManager().close(true);
		SingleGM.EndInstance();
	}

}
