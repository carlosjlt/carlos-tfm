package Variables;

import java.util.Random;

public abstract class Variable implements Comparable<Object> {
	//private static final String[] VALUES = { "ZEN", "GARNET", "LUD", "KFM" };
	
	protected String NAME;
	protected String[] _VALUES;
	private String selectedValue;
	
	private static Random rnd = new Random();
	
	//------------------------------------------------
	//------------------------------------------------
	public void InitRandomly()
	{
		this.SetSelectedValue(_VALUES[Math.abs(rnd.nextInt()) % _VALUES.length]);		
	}

	//------------------------------------------------
	//------------------------------------------------
	public String GetValueAt(int i)
	{
		if(i < _VALUES.length)
			return _VALUES[i];
		
		return null;
	}

	//------------------------------------------------
	//------------------------------------------------
	public int GetNumValues()
	{
		return _VALUES.length;
	}

	//------------------------------------------------
	//------------------------------------------------
	public String GetName()
	{
		return NAME;
	}

	//------------------------------------------------
	//------------------------------------------------
	public String GetSelectedValue()
	{
		return selectedValue;
	}

	//------------------------------------------------
	//------------------------------------------------
	public void SetSelectedValue(String _value)
	{
		for (String value : _VALUES) {
			if(value == _value)
			{
				selectedValue = value;
				break;
			}
		}
	}

	//------------------------------------------------
	//------------------------------------------------
	public String toString()
	{
		return NAME + " is " + selectedValue;
	}
	

	@Override
	public int compareTo(Object paramT) {
		return this.toString().compareTo(paramT.toString());
	}

		
}
