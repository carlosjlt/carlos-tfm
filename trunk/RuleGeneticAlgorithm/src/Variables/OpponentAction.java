package Variables;

import enumerate.Action;

public class OpponentAction extends Variable {
	
	public OpponentAction()
	{
		NAME = "OPP_ACTION";
		
		Action[] states = Action.values();
		_VALUES = new String[states.length];
	    
	    for (int i = 0; i < states.length; i++) {
	    	_VALUES[i] = states[i].name();
	    }
	}
    
}
