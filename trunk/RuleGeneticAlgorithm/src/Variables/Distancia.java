package Variables;

public class Distancia extends Variable {
	
	public static final String[] VALUES = { "VERY_FAR", "FAR", "CLOSE", "VERY_CLOSE" };
	
	public Distancia()
	{
		NAME = "DISTANCIA";
		_VALUES = VALUES;
	}
}
