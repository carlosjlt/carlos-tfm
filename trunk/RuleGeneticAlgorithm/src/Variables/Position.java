package Variables;

public class Position extends Variable {
	public static final String[] VALUES = { "CENTER", "SIDE" };

	public Position(boolean own)
	{
		if (own)
			NAME = "OWN_POSITION";
		else
			NAME = "OPP_POSITION";
			
		_VALUES = VALUES;
	}

}
