package Variables;

public class Score extends Variable {
	
	public static final String[] VALUES = { "LOSE", "DRAW", "WIN" };
	
	public Score()
	{
		NAME = "SCORE";
		_VALUES = VALUES;
	}
}
