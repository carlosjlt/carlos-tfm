package Variables;

public class Energy extends Variable {
	public static final String[] VALUES = { "NONE", "LOW", "GOOD", "FULL" };
	
	public Energy(boolean ownEnergy)
	{
		if (ownEnergy)
			NAME = "OWN_ENERGY";
		else
			NAME = "OPP_ENERGY";
			
		_VALUES = VALUES;
	}

}
