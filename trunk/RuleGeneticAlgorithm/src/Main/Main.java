package Main;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import GeneticAlgorithm.GeneticAlgorithm;
import GeneticAlgorithm.GeneticAlgorithmBase;
import GeneticAlgorithm.GeneticAlgorithmGPL;
import GeneticAlgorithm.GeneticAlgorithmTournament;
import Propio.FactoryVariables;
import Propio.IndividuoPropio;
import Propio.Regla;
import Utils.MiSystemOut;
import Variables.Variable;

public class Main {

	// -------------------------------------------------------
	// -------------------------------------------------------
	public static void main(String[] args) throws Exception {

		System.setErr(new PrintStream(new OutputStream() {
			public void write(int b) {
			}
		}));

		CleanReplays();
		CleanRules();
		CleanScores();

		/*
		 * GeneticAlgorithmBase ga = new GeneticAlgorithmRules("Rules");
		 * ga.InitDisplays(); ga.InitPopultaion(); RunGA(ga);
		 * ga.PrintIndividuals(); ga.Dispose(); ga.DisposeDisplays();
		 */

		// ---------------------------------------------------------------------------
		GeneticAlgorithmBase ga;
		GeneticAlgorithmBase.InitDisplays();
//		for (int i = 0; i < 40; ++i) {
			// GA propio
			CleanReplays();
//			CleanRules();
			CleanScores();

			ga = new GeneticAlgorithmTournament("TournMultTiempoReal" + 1 ); //i);
//			ga = new GeneticAlgorithm("PropioTiempoReal" + 0 ); //i);
//			ga = new GeneticAlgorithmGPL("GPL_" + 0 ); //i);
			ga.InitPopultaion();

			// * ONLY FOR GeneticAlgorithm Propio
//			ga.InsertIndividual(GetFackedInidivdual());
//			ga.InsertIndividual(GetFackedInidivdual());
//			ga.InsertIndividual(GetFackedInidivdual());
			// * /

			RunGA(ga);

			// ga.PrintIndividuals();

			ga.Dispose();
	/*		
			CleanReplays();
			CleanRules();
			CleanScores();

			ga = new GeneticAlgorithmGPL("GPL_" + i);
			ga.InitPopultaion();

			RunGA(ga);

			ga.PrintIndividuals();

			ga.Dispose();
*/
//		}

		// ---------------------------------------------------------------------------
/*		// GA GPL
		for (int i = 0; i < 40; ++i) {
			CleanReplays();
			CleanRules();
			CleanScores();

			ga = new GeneticAlgorithmGPL("GPL_" + i);
			ga.InitPopultaion();

			RunGA(ga);

			ga.PrintIndividuals();

			ga.Dispose();
		}
		*/
		// ---------------------------------------------------------------------------
/*
		ga = new GeneticAlgorithm("Inmigracion4");
		ga.InitPopultaion();
		ga.NEW_INDVIS_NEXT_GEN = 4;

		// * ONLY FOR GeneticAlgorithm Propio
		ga.InsertIndividual(GetFackedInidivdual());
		ga.InsertIndividual(GetFackedInidivdual());
		ga.InsertIndividual(GetFackedInidivdual());

		RunGA(ga);

		ga.PrintIndividuals();

		ga.Dispose();

		ga.DisposeDisplays();
		*/
		// ---------------------------------------------------------------------------
	}

	// ---------------------------------------------------------------------------
	public static void RunGA(GeneticAlgorithmBase ga) throws IOException {
		int NUM_ITER = 1000;

		long initTime = System.currentTimeMillis();
		long currentTime;
		for (int i = 0; i < NUM_ITER; ++i) {
			MiSystemOut.out.print("Iteration " + i + "... ");
			ga.Step();
			currentTime = System.currentTimeMillis();
			MiSystemOut.out.println((currentTime - initTime) + " ms");
			initTime = currentTime;
		}

	}

	public static void CleanReplays() {
		CleanFolder("./log/replay");
	}

	public static void CleanScores() {
		CleanFolder("./log/point");
	}

	public static void CleanRules() {
		CleanFolder("./data/aiData/Rules");
	}

	private static void CleanFolder(String dirPath) {
		MiSystemOut.out.print("Limpiando " + dirPath + "... ");
		try {
			File dir = new File(dirPath);
			File[] files = dir.listFiles();
			for (File f : files)
				f.delete();
		} catch (Exception ex) {
		}
		MiSystemOut.out.println("done");
	}

	// -------------------------------------------------------
	// -------------------------------------------------------
	private static IndividuoPropio GetFackedInidivdual() {
		IndividuoPropio ind = new IndividuoPropio();
		Variable distancia = FactoryVariables.GetInstance().GetVariable(
				"DISTANCIA");
		distancia.SetSelectedValue("FAR");
		Variable hMove = FactoryVariables.GetInstance().GetMovement(
				"HorizontalMove");
		hMove.SetSelectedValue("FORWARD");
		ind.AddRegla(new Regla(distancia, hMove, false));

		Variable distanciaC = FactoryVariables.GetInstance().GetVariable(
				"DISTANCIA");
		distanciaC.SetSelectedValue("CLOSE");
		ind.AddRegla(new Regla(distanciaC, hMove, false));

		Variable distanciaVC = FactoryVariables.GetInstance().GetVariable(
				"DISTANCIA");
		distanciaVC.SetSelectedValue("VERY_CLOSE");
		Variable dale = FactoryVariables.GetInstance().GetSkill("CROUCH_FB");
		dale.SetSelectedValue("DO_IT");
		ind.AddRegla(new Regla(distanciaVC, dale, false));

		return ind;
	}

}
