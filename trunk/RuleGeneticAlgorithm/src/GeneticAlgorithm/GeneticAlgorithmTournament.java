package GeneticAlgorithm;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import Fakes.FakeGame;
import Fakes.Sincro;
import FuzzyIA.RuleFilesManager;
import Propio.IndividuoPropio;
import Statistics.GlobalStatistics;
import Statistics.RoundStatisticsBase;
import Utils.MiSystemOut;
import Utils.SingleRandom;

public class GeneticAlgorithmTournament extends GeneticAlgorithmBase {

	public final double MUTATION_PROB = 0.5;
	public final double REDUCTION_PROB = 1.0;
	public int INIT_NUM_RULES = 35;
	public int NUM_FIGHTS = 5;

	public GeneticAlgorithmTournament(String name) {
		super(name);
		RULE_STATS_FOLDER = "./Statistics/Rules/";
		FIGHTERS_STATS_FOLDER = "./Statistics/Fighters/";
		GlobalStatistics.GetInst().InitStatistics(
				"./Statistics/PropioTournament_" + year + month + day + "_"
						+ NUM_POPULATION + "_" + this.name + ".txt");

		BEST_INDVIS_NEXT_GEN = (int) (NUM_POPULATION * 0.75);
		NUM_GAMES = NUM_POPULATION; // / 2;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void InitPopultaion() {
		for (int i = 0; i < NUM_POPULATION; ++i) {
			IndividuoPropio ip = new IndividuoPropio(INIT_NUM_RULES);
			fileToIndiv.add(ip);
			String p1 = (i < 10) ? "0" + String.valueOf(i) : String.valueOf(i);
			ip.SaveToFile(p1 + RuleFilesManager.END_INDIV);
			ip.SaveToFile(p1 + RuleFilesManager.END_SPARRING);
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void GenerateNewPopulation() throws IOException {

		GetFitness();

		IndividuoBase[] toCross = GetBestIndividuals();
		List<IndividuoBase> toCrossList = Arrays.asList(toCross);
		Collections.shuffle(toCrossList);

		// Cross
		while (newPopulation.size() < NUM_POPULATION * 0.75) {
			int m1 = SingleRandom.Rand().nextInt(toCross.length);
			int m2 = 0;
			do
			{
				m2 = SingleRandom.Rand().nextInt(toCross.length);
			}
			while (m2 != m1);
			IndividuoBase[] children = toCrossList.get(m1).Cross(
					toCrossList.get(m2));
			newPopulation.add(children[0]);
			newPopulation.add(children[1]);
		}

		int n = -1;
		while (newPopulation.size() < NUM_POPULATION) {
			IndividuoBase indiv = toCross[++n];
			// newPopulation.add(indiv);
			boolean boolRand = SingleRandom.Rand().nextBoolean();
			// Mutation
			if (boolRand) {
				newPopulation.add(indiv.Mutation());
			}
			// Reduction
			else {
				newPopulation.add(((IndividuoPropio) indiv).Reduction());
			}
		}

	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void GetFitness() throws IOException {

		RoundStatisticsBase rs = new RoundStatisticsBase(fileToIndiv);
		indivToPoint.clear();
		RuleFilesManager.GetInstance().ResetThreadsToRuleSetFile();
		RuleFilesManager.GetInstance().Reset();

		int i = 0;
		for (int fights = 0; fights < NUM_FIGHTS; ++fights) {

			Stack<Integer> selec = new Stack<Integer>();
			int pos = -1;
			while (pos < NUM_POPULATION - 1)
				selec.add(++pos);

			Collections.shuffle(selec);

			while (!selec.isEmpty()) {
				// Primer indiv
				int pos1 = selec.pop();
				IndividuoBase ind1 = fileToIndiv.get(pos1);

				// Segundo indiv
				int pos2 = selec.pop();
				IndividuoBase ind2 = fileToIndiv.get(pos2);

				String p1 = (pos1 < 10) ? "0" + String.valueOf(pos1) : String
						.valueOf(pos1);
				String p2 = (pos2 < 10) ? "0" + String.valueOf(pos2) : String
						.valueOf(pos2);
				ind1.SaveToFile(p1 + RuleFilesManager.END_INDIV);
				ind2.SaveToFile(p2 + RuleFilesManager.END_SPARRING);
				// Cargamos los nombres de los ficheros para pasarlos luego
				// RuleFilesManager.GetInstance().Reset();
				RuleFilesManager.GetInstance().SetInitFile(pos1 - 1, true);
				RuleFilesManager.GetInstance().SetInitFile(pos2 - 1, false);

				pullDisplays[i].SetParams(new FakeGame(
						new String[] { "--a1", "FuzzyGA", "--a2", "FuzzyGA",
								"--c1", OWN_TRAINING_CHARACTER, "--c2",
								OWN_TRAINING_CHARACTER }), 960, 640, 60, i);
				threads[i] = new Thread(pullDisplays[i], "Display"
						+ String.valueOf(fights * (NUM_POPULATION / 2) + i));
				int k = Sincro.Inst.AddGame();
				Sincro.Inst.ResetDone();
				threads[i].start();
				
				String sNumGames = (Sincro.Inst.GetNumGames() < 100) ? "00" + String.valueOf(Sincro.Inst.GetNumGames()) : String.valueOf(Sincro.Inst.GetNumGames());
				MiSystemOut.out.println(sNumGames);

				while (!Sincro.Inst.GetDone() /* && Sincro.Inst.GetNumGames() == k */) {
					Sleep(2);
				}

				while (Sincro.Inst.GetNumGames() > MaxGames) {
					Sleep(2);
				}
				++i;
			}

		}

		for (i = 0; i < NUM_GAMES; ++i) {
			try {
				threads[i].join();
				threads[i] = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		ReadResults();

		rs.SetPopulationFitness(this.statisticPopulation);
		rs.ToFile(FIGHTERS_STATS_FOLDER + "Tourn_Round" + year + month + day
				+ "_" + this.name + "_" + String.valueOf(++logDataCount)
				+ ".txt");
		INIT_NUM_RULES = (int) rs.getMeanRuleNumber();

		GlobalStatistics.GetInst().AddRoundStatistics(rs);

	}

}
