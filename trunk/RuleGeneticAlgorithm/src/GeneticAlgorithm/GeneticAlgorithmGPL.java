package GeneticAlgorithm;

import grammar.GrammarException;

import java.io.File;
import java.io.IOException;

import FuzzyIA.RuleFilesManager;
import GPL.IndividuoGPL;
import Statistics.GlobalStatistics;
import Statistics.RoundStatisticsBase;
import Utils.SingleRandom;

public class GeneticAlgorithmGPL extends GeneticAlgorithmBase {

	public final double MUTATION_PROB = 0.5;

	// ------------------------------------------------
	// ------------------------------------------------
	public GeneticAlgorithmGPL(String name) {
		super(name);
		RULE_STATS_FOLDER = "./Statistics/Rules/";
		FIGHTERS_STATS_FOLDER = "./Statistics/Fighters/GPL/";
		GlobalStatistics.GetInst().InitStatistics(
				"./Statistics/GlobalGPL_" + year + month + day + "_"
						+ NUM_POPULATION + "_" + this.name + ".txt");
		String path = "./data/aiData/Rules";
		File dir = new File(path);
		File[] files = dir.listFiles();
		for (File f : files)
			f.delete();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void InitPopultaion() {
		for (int i = 0; i < NUM_POPULATION; ++i) {
			try {
				fileToIndiv.add(new IndividuoGPL());
			} catch (GrammarException e) {
				e.printStackTrace();
			}
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void GenerateNewPopulation() throws IOException {
		// GetFitness
		GetFitness();

		// Vector de ruleta
		int[] acum = new int[NUM_POPULATION];
		int iAcum = 0;
		for (int i = 0; i < NUM_POPULATION; ++i) {
			int fit = 0;
			if (indivToPoint.containsKey(String.valueOf(i))) {
				fit = indivToPoint.get(String.valueOf(i));
			}

			iAcum += fit;
			acum[i] = iAcum;
		}

		// Best
		IndividuoBase[] best = GetBestIndividuals();
		for (IndividuoBase indiv : best) {
			newPopulation.add(indiv);
		}

		// Cross
		while (newPopulation.size() < NUM_POPULATION
				&& newPopulation.size() < NUM_CHILDREN) {
			IndividuoBase[] toCross = GetInidividuosToCross(acum);
			IndividuoBase[] children = toCross[0].Cross(toCross[1]);
			newPopulation.add(children[0]);
			newPopulation.add(children[1]);
		}

		// Mutation
		if (newPopulation.size() < NUM_POPULATION
				&& SingleRandom.Rand().nextDouble() < MUTATION_PROB) {
			int indiv = SingleRandom.Rand().nextInt(newPopulation.size());
			newPopulation.add(newPopulation.elementAt(indiv).Mutation());
		}

		// A�adimos nuevos individuos aleatorios para a�adir diversidad gen�tica
		try {
			while (newPopulation.size() < NUM_POPULATION) {
				newPopulation.add(new IndividuoGPL());
			}
		} catch (GrammarException e) {
			e.printStackTrace();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void GetFitness() throws IOException {

		indivToPoint.clear();

		// Individuos a fichero
		for (int i = 0; i < fileToIndiv.size(); ++i) {
			fileToIndiv.elementAt(i).SaveToFile(String.valueOf(i) + RuleFilesManager.END_INDIV);
		}

		// Cargamos los nombres de los ficheros para pasarlos luego
		RuleFilesManager.GetInstance().Reset();

		RoundStatisticsBase rs = new RoundStatisticsBase(fileToIndiv);

		RuleFilesManager.GetInstance().ResetThreadsToRuleSetFile();
		LaunchGames();
		ReadResults();

		rs.SetPopulationFitness(this.statisticPopulation);
		rs.ToFile(FIGHTERS_STATS_FOLDER + "GPL_Round" + year + month + day
				+ "_" + this.name + "_" + String.valueOf(++logDataCount)
				+ ".txt");

		GlobalStatistics.GetInst().AddRoundStatistics(rs);
	}

}
