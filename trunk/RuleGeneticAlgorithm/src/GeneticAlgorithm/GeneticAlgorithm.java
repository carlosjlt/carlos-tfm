package GeneticAlgorithm;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import Fakes.FakeDisplay;
import Fakes.FakeGame;
import FuzzyIA.RuleFilesManager;
import Propio.IndividuoPropio;
import Statistics.GlobalStatistics;
import Statistics.RoundStatisticsBase;
import Utils.SingleRandom;

public class GeneticAlgorithm extends GeneticAlgorithmBase {

	public final double MUTATION_PROB = 0.5;
	public final double REDUCTION_PROB = 1.0;
	public int INIT_NUM_RULES = 35;

	public GeneticAlgorithm(String name) {
		super(name);
		RULE_STATS_FOLDER = "./Statistics/Rules/";
		FIGHTERS_STATS_FOLDER = "./Statistics/Fighters/";
		GlobalStatistics.GetInst().InitStatistics(
				"./Statistics/GlobalPropio_" + year + month + day + "_"
						+ NUM_POPULATION + "_" + this.name + ".txt");

		/*
		String path = "./data/aiData/Rules";
		File dir = new File(path);
		File[] files = dir.listFiles();
		for (File f : files)
			f.delete();
			*/
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void InitPopultaion() {
		for (int i = 0; i < NUM_POPULATION; ++i) {
			fileToIndiv.add(new IndividuoPropio(INIT_NUM_RULES));
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void GenerateNewPopulation() throws IOException {
		// GetFitness
		GetFitness();

		// Vector de ruleta
		int[] acum = new int[NUM_POPULATION];
		int iAcum = 0;
		for (int i = 0; i < NUM_POPULATION; ++i) {
			int fit = 0;
			if (indivToPoint.containsKey(String.valueOf(i))) {
				fit = indivToPoint.get(String.valueOf(i));
			}
			iAcum += fit;
			acum[i] = iAcum;
		}

		// Best
		IndividuoBase[] best = GetBestIndividuals();
		for (IndividuoBase indiv : best) {
			newPopulation.add(indiv);
		}

		for (IndividuoBase indiv : best) {
			boolean boolRand = SingleRandom.Rand().nextBoolean();
			// Mutation
			if (boolRand && newPopulation.size() < NUM_POPULATION) {
				newPopulation.add(indiv.Mutation());
			}

			// Reduction
			if (!boolRand && newPopulation.size() < NUM_POPULATION) {
				newPopulation.add(((IndividuoPropio) indiv).Reduction());
			}

		}

		// Cross
		while (newPopulation.size() < NUM_POPULATION - NEW_INDVIS_NEXT_GEN) {
			// && newPopulation.size() < NUM_CHILDREN) {
			IndividuoBase[] toCross = GetInidividuosToCross(acum);
			IndividuoBase[] children = toCross[0].Cross(toCross[1]);
			newPopulation.add(children[0]);
			newPopulation.add(children[1]);
		}

		/*
		 * // Mutation if (newPopulation.size() < NUM_POPULATION &&
		 * SingleRandom.Rand().nextDouble() < MUTATION_PROB) { int indiv =
		 * SingleRandom.Rand().nextInt(newPopulation.size());
		 * newPopulation.add(newPopulation.elementAt(indiv).Mutation()); }
		 * 
		 * // Reduction if (newPopulation.size() < NUM_POPULATION &&
		 * SingleRandom.Rand().nextDouble() < REDUCTION_PROB) { int indiv =
		 * SingleRandom.Rand().nextInt(newPopulation.size());
		 * newPopulation.add(((IndividuoPropio)
		 * newPopulation.elementAt(indiv)).Reduction()); }
		 */

		// A�adimos nuevos individuos aleatorios para a�adir diversidad gen�tica
		while (newPopulation.size() < NUM_POPULATION) {
			newPopulation.add(new IndividuoPropio(INIT_NUM_RULES));
		}

	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void GetFitness() throws IOException {

		indivToPoint.clear();

		// Individuos a fichero
		for (int i = 0; i < fileToIndiv.size(); ++i) {
			String p1 = (i < 10) ? "0" + String.valueOf(i) : String.valueOf(i);
			fileToIndiv.elementAt(i).SaveToFile(p1 + RuleFilesManager.END_INDIV);
		}

		// Cargamos los nombres de los ficheros para pasarlos luego
		RuleFilesManager.GetInstance().Reset();

		RoundStatisticsBase rs = new RoundStatisticsBase(fileToIndiv);

		RuleFilesManager.GetInstance().ResetThreadsToRuleSetFile();
		LaunchGames();
		ReadResults();

		rs.SetPopulationFitness(this.statisticPopulation);
		rs.ToFile(FIGHTERS_STATS_FOLDER + "Round" + year + month + day + "_"
				+ this.name + "_" + String.valueOf(++logDataCount) + ".txt");
		INIT_NUM_RULES = (int) rs.getMeanRuleNumber();

		GlobalStatistics.GetInst().AddRoundStatistics(rs);
	}

}
