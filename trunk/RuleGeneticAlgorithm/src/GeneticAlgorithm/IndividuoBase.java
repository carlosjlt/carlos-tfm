package GeneticAlgorithm;

import java.io.IOException;


public abstract class IndividuoBase {

	public IndividuoBase() {}

	public abstract IndividuoBase Mutation();

	public abstract IndividuoBase[] Cross(IndividuoBase padre2);
	
	public abstract void SaveToFile(String fileName) throws IOException;
	
	public abstract int getNumRules();
	
	public abstract String[] getRuleList();
}
