package GeneticAlgorithm;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Vector;

import Fakes.FakeDisplay;
import Fakes.FakeGame;
import Fakes.Sincro;
import FuzzyIA.RuleFilesManager;
import Main.Main;
import SingleManagers.SingleGM;
import SingleManagers.SingleImageContainer;
import SingleManagers.SingleSoundManager;
import Statistics.IndividualStatistics;
import Utils.MiSystemOut;
import Utils.SingleRandom;

public abstract class GeneticAlgorithmBase {

	// T3c, DragonKing3C, PnumaSON3C_AI, FuzzyGA(-> Entre ellos)
	/*
	 * public static final String[] OPPONENTS = { "NoAction", "NoAction",
	 * "NoAction", "T3c", "DragonKing3C", "PnumaSON3C_AI", "FuzzyGA", "FuzzyGA",
	 * "FuzzyGA", "FuzzyGA", "FuzzyGA", "FuzzyGA", "FuzzyGA", "FuzzyGA",
	 * "FuzzyGA", "FuzzyGA", "FuzzyGA", "FuzzyGA", "FuzzyGA", "FuzzyGA",
	 * "FuzzyGA", "FuzzyGA", "FuzzyGA" }; // Un // cuarto // de no // hacer //
	 * nada
	 */
	public static final String[] OPPONENTS = { "SparringRandom", "NoAction",
			"T3c", "DragonKing3C", "PnumaSON3C_AI" };

	// public static final String[] OPPONENTS = { "FuzzyGA" };
	public static final String TRAINING_OPPONENT = "T3c";
	// GARNET, LUD, ZEN
	public static final String[] CHARACTERS = { "GARNET", "LUD", "ZEN" };
	public static final String OPPONENT_TRAINING_CHARACTER = "LUD";
	public static final String OWN_TRAINING_CHARACTER = "ZEN";

	public String name;

	public final int NUM_POPULATION = 100; // Debe ser divisible entre 4!
	public int BEST_INDVIS_NEXT_GEN = 2;
	public int NEW_INDVIS_NEXT_GEN = 2;
	public int NUM_CHILDREN = NUM_POPULATION - BEST_INDVIS_NEXT_GEN
			- NEW_INDVIS_NEXT_GEN;

	public int NUM_GAMES = NUM_POPULATION * OPPONENTS.length;

	public static final int PENALIZ_POR_REGLA = -10;

	// Cada combate tiene un m�ximo de 3000 puntos.
	// Si no se enfrentan entre ellos, cada Game es un combate de un solo
	// individuo.
	public final int MAX_FITNESS = 3000 * (NUM_GAMES / NUM_POPULATION);
	public final double NORMALIZATION_FACTOR = 1000.0 / MAX_FITNESS; // / 2; //
																		// El /
																		// 2 es
																		// cuando
																		// sea
																		// entre
																		// los
																		// FuzzyGA

	// Deben inicializarlo las clases
	public static String RULE_STATS_FOLDER;
	public static String FIGHTERS_STATS_FOLDER;

	public static final int MAX_PROCESS_CALLS = 10800;
	public static int MaxGames = 1000;

	private long threadsDelay = 100;
	private boolean calcDelays = true;
	private static final int RECALC = 5;
	private int countdowntRecalc = RECALC;

	int logDataCount = 0;
	Calendar cal1 = Calendar.getInstance();
	int year = cal1.get(1);
	int month = cal1.get(2) + 1;
	int day = cal1.get(5);

	Vector<IndividuoBase> fileToIndiv = new Vector<IndividuoBase>();
	Vector<IndividuoBase> newPopulation = new Vector<IndividuoBase>();
	Vector<IndividualStatistics> statisticPopulation = new Vector<IndividualStatistics>();
	IndividualStatistics fooStats = new IndividualStatistics();

	Vector<FakeGame> pullGames = new Vector<FakeGame>();
	FakeDisplay[] pullDisplays;
	Thread[] threads;
	HashMap<String, Integer> indivToPoint;

	// -------------- Abstact Methods -----------------

	public abstract void InitPopultaion();

	public abstract void GenerateNewPopulation() throws IOException;

	public abstract void GetFitness() throws IOException;

	// ------------ End Abstact Methods ---------------

	public final void Step() throws IOException {
		for (IndividualStatistics is : this.statisticPopulation)
			is.clear();

		GenerateNewPopulation();

		if (calcDelays) {
			int diff = getDiffNumGames();
			if (diff == 0) {
				calcDelays = false;
				RuleFilesManager.GetInstance().setSaveProcessCalls(false);
				this.countdowntRecalc = RECALC;
			} else {
				MaxGames += diff;
				if (MaxGames < 1)
					MaxGames = 1;

			}

		} else {
			if ((--countdowntRecalc) == 0) {
				calcDelays = true;
				RuleFilesManager.GetInstance().setSaveProcessCalls(true);
			}
		}
		MiSystemOut.out.println("Games paralelos: " + MaxGames);

		fileToIndiv.clear();
		fileToIndiv.addAll(newPopulation);
		newPopulation.clear();
		Main.CleanReplays();
		Main.CleanScores();
	}

	// -------------- Final Methods -----------------

	public GeneticAlgorithmBase(String name) {
		MiSystemOut.out.print("Initializating GA... ");
		this.name = name;
		threads = new Thread[NUM_GAMES];
		pullDisplays = new FakeDisplay[NUM_GAMES];
		indivToPoint = new HashMap<String, Integer>();

		for (int i = 0; i < NUM_GAMES; ++i) {
			pullDisplays[i] = new FakeDisplay();
			threads[i] = new Thread(pullDisplays[i], "Display"
					+ String.valueOf(i));
		}

		for (int i = 0; i < NUM_POPULATION; ++i) {
			this.statisticPopulation.add(new IndividualStatistics());
		}

		RuleFilesManager.GetInstance().setSaveProcessCalls(true);
		MiSystemOut.out.println("done");
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public final void Dispose() {
		indivToPoint.clear();
		fileToIndiv.clear();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public static final void InitDisplays() {
		MiSystemOut.out.print("Initializating displays... ");
		SingleGM.InitInstance();
		RuleFilesManager.InitInstance();
		SingleImageContainer.InitInstance();
		MiSystemOut.out.println("done");
	}

	public static final void DisposeDisplays() {
		SingleImageContainer.GetImageContainer().finalize();
		SingleSoundManager.GetSoundManager().close(true);
		SingleGM.EndInstance();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public final void InsertIndividual(IndividuoBase indiv) {
		if (this.fileToIndiv.size() == NUM_POPULATION) {
			this.fileToIndiv.remove(0);
		}
		this.fileToIndiv.add(indiv);
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public final IndividuoBase[] GetBestIndividuals() {
		int[] pos = new int[BEST_INDVIS_NEXT_GEN];
		double[] bests = new double[BEST_INDVIS_NEXT_GEN];
		for (int i = 0; i < BEST_INDVIS_NEXT_GEN; ++i)
			bests[i] = 0.0;

		int k = 0;
		for (int i = 0; i < NUM_POPULATION; ++i) {
			if (indivToPoint.containsKey(String.valueOf(i))) {
				double val = indivToPoint.get(String.valueOf(i));
				for (int j = 0; j < BEST_INDVIS_NEXT_GEN; ++j) {
					if (val > bests[j]) {
						k = BEST_INDVIS_NEXT_GEN - 1;
						while (k > j) {
							pos[k] = pos[k - 1];
							bests[k] = bests[--k];
						}

						bests[j] = val;
						pos[j] = i;
						break;
					}
				}
			}
		}

		IndividuoBase[] results = new IndividuoBase[BEST_INDVIS_NEXT_GEN];
		for (int i = 0; i < BEST_INDVIS_NEXT_GEN; ++i) {
			results[i] = fileToIndiv.elementAt(pos[i]);
		}

		return results;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public final IndividuoBase[] GetInidividuosToCross(int[] acum) {
		int pos1 = SelectPosition(acum);
		int pos2 = SelectPosition(acum);

		return new IndividuoBase[] { fileToIndiv.elementAt(pos1),
				fileToIndiv.elementAt(pos2) };
	}

	// ------------------------------------------------
	// ------------------------------------------------
	protected final int SelectPosition(int[] acum) {
		int pos = 0;
		int randVal = SingleRandom.Rand().nextInt(acum[NUM_POPULATION - 1]);

		while (acum[pos] < randVal && pos < acum.length)
			++pos;

		return pos;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	protected final void ReadResults() {
		HashMap<String, String> DisplayToPlayer1 = new HashMap<String, String>();
		HashMap<String, String> DisplayToPlayer2 = new HashMap<String, String>();

		try {
			// Leer A que display corresponde que conjunto de reglas
			File file = new File("./data/aiData/ThreadsToRuleSet.txt");
			for (String line : Files.readAllLines(file.toPath())) {
				String[] values = line.split(" ");

				// El valor true significa que el player es el 1
				if (values[2].equalsIgnoreCase("true"))
					DisplayToPlayer1.put(values[1],
							values[0].replace(RuleFilesManager.END_INDIV, ""));
				else
					DisplayToPlayer2.put(values[1], 
							values[0].replace(RuleFilesManager.END_SPARRING, ""));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		IndividualStatistics statsPlayer1;
		IndividualStatistics statsPlayer2;
		String indiv1;
		String indiv2;
		
		String path = "./log/point";
		File dir = new File(path);
		File[] files = dir.listFiles();

		for (int i = 0; i < files.length; ++i) {
			File resultFile = files[i];

			// Actualizar score player1
			indiv1 = DisplayToPlayer1.get("Display" + String.valueOf(i));
			indiv2 = DisplayToPlayer2.get("Display" + String.valueOf(i));

			if (indiv1 != null) {
				statsPlayer1 = this.statisticPopulation.elementAt(Integer
						.parseInt(indiv1));
			} else {
				statsPlayer1 = fooStats;
			}

			if (indiv2 != null) {
				statsPlayer2 = this.statisticPopulation.elementAt(Integer
						.parseInt(indiv2));
			} else {
				statsPlayer2 = fooStats;
			}

			try {
				for (String line : Files.readAllLines(resultFile.toPath())) {
					String[] values = line.split(",");
					statsPlayer1.addResult(Integer.parseInt(values[1]));
					statsPlayer2.addResult(Integer.parseInt(values[2]));
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Actualizar score player1
			if (indiv1 != null) {
				statsPlayer1.calcMeanAndDesviation();
				indivToPoint.put(indiv1, (int) statsPlayer1.getMean());
			}

			// Actualizar score player2
			if (indiv2 != null) {
				statsPlayer2.calcMeanAndDesviation();
				indivToPoint.put(indiv2, (int) statsPlayer2.getMean());
			}

		}

		for (int i = 0; i < NUM_POPULATION; ++i) {
			IndividualStatistics is = this.statisticPopulation.elementAt(i);
			is.setNumRules(fileToIndiv.elementAt(i).getNumRules());
			indivToPoint.put(String.valueOf(i), is.getFitness());
		}

	}

	// ------------------------------------------------
	// ------------------------------------------------
	protected void LaunchGames() {
		// Llamada a los displays
		int i = -1;
		for (int j = 0; j < OPPONENTS.length; ++j) {
			for (int k = 0; k < NUM_POPULATION; ++k) {
				pullDisplays[++i].SetParams(new FakeGame(new String[] { "--a1",
						"FuzzyGA", "--a2",
						OPPONENTS[(j + i) % OPPONENTS.length], "--c1",
						OWN_TRAINING_CHARACTER, "--c2",
						OPPONENT_TRAINING_CHARACTER }), 960, 640, 60, i);
				threads[i] = new Thread(pullDisplays[i], "Display"
						+ String.valueOf(i));
			}
		}

		for (i = 0; i < NUM_GAMES; ++i) {
			int k = Sincro.Inst.AddGame();
			// MiSystemOut.out.println("NumGames: " + k);
			Sincro.Inst.ResetDone();
			threads[i].start();

			while (!Sincro.Inst.GetDone() && Sincro.Inst.GetNumGames() == k) {
				Sleep(1);
			}

			while (Sincro.Inst.GetNumGames() > MaxGames) {
				Sleep(1);
			}
		}

		for (i = 0; i < NUM_GAMES; ++i) {
			try {
				threads[i].join();
				threads[i] = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		/*
		 * if (calcDelays) {
		 * 
		 * int newDelay = getThreadsDelay(); if (newDelay != 0 && threadsDelay >
		 * 4) { threadsDelay += newDelay; } else { calcDelays = false;
		 * RuleFilesManager.GetInstance().setSaveProcessCalls(false);
		 * this.countdowntRecalc = RECALC; }
		 * MiSystemOut.out.println("Delay entre threads: " + threadsDelay); }
		 * else { if ((--countdowntRecalc) == 0) { calcDelays = true;
		 * RuleFilesManager.GetInstance().setSaveProcessCalls(true); } }
		 */
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public final void Sleep(long milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	protected final void clearStats() {
		for (IndividualStatistics is : this.statisticPopulation) {
			is.clear();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public final void PrintIndividuals() {
		int i = 0;
		for (IndividuoBase indiv : this.fileToIndiv) {
			MiSystemOut.out
					.println("------------------------------------------------------------------------");
			MiSystemOut.out.println("Individuo " + String.valueOf((++i)));
			MiSystemOut.out.println(indiv.toString());
			MiSystemOut.out
					.println("------------------------------------------------------------------------");
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private final int getDiffNumGames() {
		int mean = getMeanProcessCalls();
		MiSystemOut.out.println("Mean process calls " + mean);
		if (mean < MAX_PROCESS_CALLS - 2000)
			return -3;
		else if (mean < MAX_PROCESS_CALLS - 1000)
			return -2;
		else if (mean < MAX_PROCESS_CALLS - 500)
			return -1;
		else if (mean > MAX_PROCESS_CALLS - 200)
			return +1;

		return 0;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private final int getThreadsDelay() {
		int mean = getMeanProcessCalls();
		MiSystemOut.out.println("Mean process calls " + mean);
		if (mean < MAX_PROCESS_CALLS - 2000)
			return 50;
		else if (mean < MAX_PROCESS_CALLS - 1000)
			return 20;
		else if (mean < MAX_PROCESS_CALLS - 500)
			return 10;
		else if (mean > MAX_PROCESS_CALLS - 200)
			return -5;

		return 0;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private final int getMeanProcessCalls() {
		int mean = 0;
		File file = new File(RuleFilesManager.LLAMADAS_PROCESS_CALLS);
		try {
			int acum = 0;
			int numProcess = 0;
			for (String line : Files.readAllLines(file.toPath())) {
				acum += Integer.parseInt(line);
				++numProcess;
			}
			mean = acum / numProcess;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return mean;
	}

	// ------------ End Final Methods ---------------

}
