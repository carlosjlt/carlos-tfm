package GeneticAlgorithm;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import FuzzyIA.RuleFilesManager;
import Propio.ComplexRule;
import Propio.IndividuoPropio;
import Statistics.GlobalStatistics;
import Statistics.RoundStatisticsBase;
import Statistics.RuleStatistics;
import Utils.SingleRandom;

public class GeneticAlgorithmRules extends GeneticAlgorithmBase {

	public final double MUTATION_PROB = 0.2;
	public final double REDUCTION_PROB = 0.2;
	private final double RULE_USAGE_FACTOR = 10.0; 

	public int INIT_NUM_RULES = 5;
	

	// ------------------------------------------------
	// ------------------------------------------------
	public GeneticAlgorithmRules(String name) {
		super(name);
		RULE_STATS_FOLDER = "./Statistics/Rules/";
		FIGHTERS_STATS_FOLDER = "./Statistics/Fighters/";
		GlobalStatistics.GetInst().InitStatistics(
				"./Statistics/GlobalRules_" + year + month + day + "_"
						+ NUM_POPULATION + "_" + this.name + ".txt");
		String path = "./data/aiData/Rules";
		File dir = new File(path);
		File[] files = dir.listFiles();
		for (File f : files)
			f.delete();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void InitPopultaion() {
		for (int i = 0; i < NUM_POPULATION; ++i) {
			fileToIndiv.add(new IndividuoPropio(INIT_NUM_RULES));
		}
	}


	// ------------------------------------------------
	// ------------------------------------------------
	public void GenerateNewPopulation()  throws IOException {

		// --- GetFitness ---
		GetFighterFitness();
		HashMap<String, ComplexRule> rules = GetRulesFitness();

		// --- Cross ---
		// Crear ruleta de reglas
		Vector<ComplexRule> vectorRules = new Vector<ComplexRule>();
		double[] acum = CreateAcumulatedScores(rules, vectorRules);

		// Sacamos las estad�sticas de las reglas
		RuleStatistics rs = new RuleStatistics(vectorRules);
		rs.ToFile(RULE_STATS_FOLDER + "RulesRound" + year + month + day + "_"
				+ String.valueOf(logDataCount) + ".txt");

		// Generar individuos de INIT_NUM_RULES con ruleta sobre las relgas
		while (newPopulation.size() < NUM_CHILDREN) {
			IndividuoPropio ind = new IndividuoPropio();
			while (ind.listReglas.size() < INIT_NUM_RULES)
				ind.AddRegla(vectorRules.elementAt(SelectPosition(acum)));

			newPopulation.add(ind);
		}

		// --- Reemplazo ---
		// Se dejan los dos mejores luchadores
		IndividuoBase[] best = GetBestIndividuals();
		for (IndividuoBase indiv : best) {
			newPopulation.add(indiv);
		}

		// A�adimos nuevos individuos aleatorios para a�adir diversidad gen�tica
		while (newPopulation.size() < NUM_POPULATION) {
			newPopulation.add(new IndividuoPropio(INIT_NUM_RULES));
		}

		/*
		 * // Mutation if (SingleRandom.Rand().nextDouble() < MUTATION_PROB) {
		 * newPopulation.elementAt(0).Mutation(); }
		 * 
		 * // Reduction if (SingleRandom.Rand().nextDouble() < REDUCTION_PROB) {
		 * newPopulation.elementAt(1).Reduction(); }
		 */

		fileToIndiv.clear();
		fileToIndiv.addAll(newPopulation);
		newPopulation.clear();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private double[] CreateAcumulatedScores(HashMap<String, ComplexRule> rules,
			Vector<ComplexRule> vectorRules) {
		double[] acum = new double[rules.size()];
		int iAcum = 0;
		int i = -1;
		for (ComplexRule cx : rules.values()) {
			vectorRules.add(cx);
			iAcum += cx.GetMeanValue() + cx.GetUsages() * RULE_USAGE_FACTOR;
			acum[++i] = iAcum;
		}
		return acum;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public int[] GetRuleToCross(double[] acum) {
		int pos1 = SelectPosition(acum);
		int pos2 = SelectPosition(acum);

		return new int[] { pos1, pos2 };
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private int SelectPosition(double[] acum) {
		int pos = 0;
		int randVal = SingleRandom.Rand().nextInt((int) acum[acum.length - 1]);

		while (acum[pos] < randVal && pos < acum.length)
			++pos;

		return pos;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private HashMap<String, ComplexRule> GetRulesFitness() {
		HashMap<String, ComplexRule> rules = new HashMap<String, ComplexRule>();
		for (int i = 0; i < NUM_POPULATION; ++i) {
			if (indivToPoint.containsKey(String.valueOf(i))) {
				int indivValue = indivToPoint.get(String.valueOf(i));
				for (Comparable<Object> cr : ((IndividuoPropio)this.fileToIndiv.get(i)).listReglas) {
					if (!rules.containsKey(cr.toString())) {
						((ComplexRule) cr).Clear();
						rules.put(cr.toString(), (ComplexRule) cr);
					}
					rules.get(cr.toString()).AddValue(indivValue);
				}
			}
		}
		return rules;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void GetFighterFitness() throws IOException {

		indivToPoint.clear();

		// Individuos a fichero
		for (int i = 0; i < fileToIndiv.size(); ++i) {
			fileToIndiv.elementAt(i).SaveToFile(String.valueOf(i));
		}

		// Cargamos los nombres de los ficheros para pasarlos luego
		RuleFilesManager.GetInstance().Reset();

		RoundStatisticsBase rs = new RoundStatisticsBase(fileToIndiv);

		RuleFilesManager.GetInstance().ResetThreadsToRuleSetFile();
		LaunchGames();
		ReadResults();

		rs.SetPopulationFitness(this.statisticPopulation);
		rs.ToFile(FIGHTERS_STATS_FOLDER + "Rules_Round" + year + month + day + "_"
				+ String.valueOf(++logDataCount) + ".txt");
		// INIT_NUM_RULES = (int) rs.getMeanRuleNumber();

		GlobalStatistics.GetInst().AddRoundStatistics(rs);
	}

	@Override
	public void GetFitness() throws IOException {
		// TODO Auto-generated method stub
		
	}

}
