package Actions.Move;

public class HorizontalMove extends Move {

	public static final String[] VALUES = { "BACK", /*"STAND",*/ "FORWARD" };
	
	public HorizontalMove()
	{
		NAME = "HorizontalMove";
		_VALUES = VALUES;
	}

}
