package Actions.Move;

public class VerticalMove extends Move {

	public static final String[] VALUES = { "CROUCH", /*"STAND",*/ "JUMP" };
	
	public VerticalMove()
	{
		NAME = "VerticalMove";
		_VALUES = VALUES;
	}

}
