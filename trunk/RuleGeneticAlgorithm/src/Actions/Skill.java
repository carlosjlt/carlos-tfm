package Actions;

public class Skill extends Action {

	public static final String[] VALUES = { /*"DONT_DO",*/ "TRY", "DO_IT" };

	public static final String[] SKILLS = { "STAND_GUARD", "CROUCH_GUARD",
			"AIR_GUARD", "THROW_A", "THROW_B", "STAND_A", "STAND_B",
			"CROUCH_A", "CROUCH_B", "AIR_A", "AIR_B", "AIR_DA", "AIR_DB",
			"STAND_FA", "STAND_FB", "CROUCH_FA", "CROUCH_FB", "AIR_FA",
			"AIR_FB", "AIR_UA", "AIR_UB", "STAND_D_DF_FA", "STAND_D_DF_FB",
			"STAND_F_D_DFA", "STAND_F_D_DFB", "STAND_D_DB_BA", "STAND_D_DB_BB",
			"AIR_D_DF_FA", "AIR_D_DF_FB", "AIR_F_D_DFA", "AIR_F_D_DFB",
			"AIR_D_DB_BA", "AIR_D_DB_BB", "STAND_D_DF_FC" };

	public Skill(String skillName) {
		_VALUES = VALUES;
		NAME = skillName;
	}

	public static boolean IsValidSkill(String skillName) {
		for (String skill : SKILLS) {
			if (skillName.compareTo(skill) == 0)
				return true;
		}
		return false;
	}

}
