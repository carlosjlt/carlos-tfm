package Statistics;

import java.io.PrintStream;
import java.util.Vector;

import GeneticAlgorithm.GeneticAlgorithmBase;

public class IndividualStatistics {

	Vector<Integer> fightResults = new Vector<Integer>();
	double meanFitness;
	double stdDesviation;
	int minFight = 1000;
	int maxFight = 0;
	boolean validData = false;
	int currentFit = 0;
	int numRules = 0;

	public IndividualStatistics() {
	}

	public double getMean() {
		return meanFitness;
	}

	public double getStandardDesviation() {
		return stdDesviation;
	}

	public int getMaxFight() {
		return maxFight;
	}

	public int getMinFight() {
		return minFight;
	}

	public boolean isValidData() {
		return validData;
	}

	public void clear() {
		fightResults.clear();
		meanFitness = 0.0;
		stdDesviation = 0.0;
		minFight = 1000;
		maxFight = 0;
	}

	public void addResult(int result) {
		fightResults.add(result);

		if (result > maxFight)
			maxFight = result;

		if (result < minFight)
			minFight = result;

		validData = false;
	}

	public int getFitness() {
		if (!validData) {
			calcMeanAndDesviation();
		}
		
		currentFit = (int)meanFitness; // - (int)stdDesviation/4;
//		currentFit -= numRules * GeneticAlgorithmBase.PENALIZ_POR_REGLA;
		if (currentFit < 0)
			currentFit = 0;
		
		return currentFit;
	}
	
	public void setNumRules(int _numRules)
	{
		this.numRules = _numRules;
		this.validData = false;
	}

	public void calcMeanAndDesviation() {
		// Media
		int acum = 0;
		for (Integer val : fightResults) {
			acum += val;
		}
		this.meanFitness = acum / fightResults.size();

		// Desviacion tipica
		acum = 0;
		for (Integer val : fightResults) {
			acum += (val - meanFitness) * (val - meanFitness);
		}
		this.stdDesviation = Math.sqrt((acum / fightResults.size()));

		validData = true;
	}

	public void Print(PrintStream pw) {
		pw.print(currentFit + "\t" + meanFitness + "\t" + stdDesviation + "\t" + maxFight + "\t"
				+ minFight);
	}
}
