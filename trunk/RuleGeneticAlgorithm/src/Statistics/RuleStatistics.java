package Statistics;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

import Propio.ComplexRule;

public class RuleStatistics {

	Vector<ComplexRule> rules;

	public RuleStatistics(Vector<ComplexRule> rules) {
		this.rules = rules;
	}

	public void Show() {
		Print(System.out);
	}

	public void ToFile(String path) {
		File file;
		file = new File(path);
		try {
			PrintStream ps = new PrintStream(file);
			Print(ps);
			ps.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void Print(PrintStream pw) {
		pw.println("Number of rules:");
		pw.println("  Rules: " + this.rules.size());
		pw.println("----------------------------------------------");
		pw.println("Rule\tUsage\tMean Fitness");
		for (Comparable<Object> rule : rules) {
			pw.println(rule.toString() + "\t"
					+ ((ComplexRule) rule).GetUsages() + "\t"
					+ ((ComplexRule) rule).GetMeanValue());
		}
	}
}
