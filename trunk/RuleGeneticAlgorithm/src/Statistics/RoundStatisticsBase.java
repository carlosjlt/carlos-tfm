package Statistics;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Vector;

import GeneticAlgorithm.IndividuoBase;
import Utils.MiSystemOut;

public class RoundStatisticsBase {
	double meanRuleNumber;
	double minRuleNumber = 1000;
	double maxRuleNumber = 0;
	int bestRuleNumber = 0;
	int numInidvs = 0;
	IndividuoBase bestInd;
	double meanFitness = 0;
	double minFitness = 100000;
	double maxFitness = 0;

	double meanFightMeans = 0;
	double minFightMeans = 100000;
	double maxFightMeans = 0;

	double meanStdDesviation;
	double stdDesvStdDesviation;
	
	HashMap<Integer, Integer> numRulesMap = new HashMap<Integer, Integer>();

	Vector<IndividuoBase> population;
	Vector<IndividualStatistics> indivStats;

	// Individuo bestIndividual;
	HashMap<String, Integer> ruleUses = new HashMap<String, Integer>();
	Vector<String> rules = new Vector<String>();

	// ------------------------------------------------
	// ------------------------------------------------
	public RoundStatisticsBase(Vector<IndividuoBase> fileToIndiv) {

		this.population = fileToIndiv;
		int acumNumRules = 0;
		numInidvs = fileToIndiv.size();
		for (IndividuoBase indiv : fileToIndiv) {
			int numRules = indiv.getNumRules();

			if (!numRulesMap.containsKey(numRules)) {
				numRulesMap.put(numRules, 0);
			}
			numRulesMap.put(numRules, numRulesMap.get(numRules) + 1);

			acumNumRules += numRules;
			if (numRules < minRuleNumber)
				minRuleNumber = numRules;

			else if (numRules > maxRuleNumber)
				maxRuleNumber = numRules;

			for (String stringRule : indiv.getRuleList()) {
				if (!ruleUses.containsKey(stringRule)) {
					rules.add(stringRule);
					ruleUses.put(stringRule, 0);
				}
				ruleUses.put(stringRule, ruleUses.get(stringRule) + 1);
			}

		}
		meanRuleNumber = acumNumRules / fileToIndiv.size();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void SetPopulationFitness(Vector<IndividualStatistics> _indivStats) {
		indivStats = _indivStats;
		double fitnessAcum = 0.0;
		double fightAcum = 0.0;
		double stdDesviationAcum = 0.0;
		for (int i = 0; i < this.population.size(); ++i) {
			IndividualStatistics is = indivStats.elementAt(i);
			
			// Fitness
			fitnessAcum += is.getFitness();
			if (is.getFitness() > this.maxFitness) {
				this.bestInd = this.population.get(i);
				this.maxFitness = is.getFitness();
			} else if (is.getFitness() < this.minFitness) {
				this.minFitness = is.getFitness();
			}
			stdDesviationAcum += is.getStandardDesviation();
			
			// FightResults
			fightAcum += is.getMean(); 
			if (is.getMean() > this.maxFightMeans) {
				this.maxFightMeans = is.getMean();
			} else if (is.getMean() < this.minFightMeans) {
				this.minFightMeans = is.getMean();
			}
			
		}
		bestRuleNumber = this.bestInd.getNumRules();
		this.meanFitness = fitnessAcum / this.population.size();
		this.meanFightMeans = fightAcum / this.population.size();
		this.meanStdDesviation = stdDesviationAcum / this.population.size();
		
		double acum = 0.0;
		for (IndividualStatistics is : this.indivStats) {
				acum += (is.getStandardDesviation() - meanStdDesviation) * (is.getStandardDesviation() - meanStdDesviation);
		}
		this.stdDesvStdDesviation = Math.sqrt(acum / this.population.size());
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public double getMeanRuleNumber() {
		return meanRuleNumber;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public double getMinRuleNumber() {
		return minRuleNumber;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public double getMaxRuleNumber() {
		return maxRuleNumber;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void Show() {
		Print(MiSystemOut.out);
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void ToFile(String path) {
		File file;
		file = new File(path);
		try {
			PrintStream ps = new PrintStream(file);
			Print(ps);
			ps.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private void Print(PrintStream pw) {
		pw.println("Number of rules:");
		for (Integer key : numRulesMap.keySet()) {
			pw.println("  Rule Number: " + key + " -> " + numRulesMap.get(key));
		}
		pw.println("  Mean Rule Number: " + meanRuleNumber);
		pw.println("  Min Rule Number:  " + minRuleNumber);
		pw.println("  Max Rule Number:  " + maxRuleNumber);
		pw.println("----------------------------------------------");
		pw.println("Rule usage:");
		for (String rule : rules) {
			pw.println(rule + " -> \t"
					+ (100.0 * ruleUses.get(rule) / numInidvs) + "%");
		}
		pw.println("----------------------------------------------");
		pw.println("Fitness results:");

		int pos = -1;
		pw.println("Indiv => \tcurrentFit\tmeanFitness\tstdDesviation\tmaxFight\tminFight");
		for (IndividualStatistics is : this.indivStats) {
			pw.print((++pos) + " => \t");
			is.Print(pw);
			pw.println();
		}
		pw.println("Population fitnes -> " + this.meanFitness);
		pw.println("  Mean : " + meanFitness);
		pw.println("  MeanStdDesviation : " + meanStdDesviation);
		pw.println("  StdDesvStdDesviation : " + stdDesvStdDesviation);
		pw.println("  Min  : " + minFitness);
		pw.println("  Max  : " + maxFitness);
		pw.println("  Mean Fights : " + meanFightMeans);
		pw.println("  Min Fights  : " + minFightMeans);
		pw.println("  Max Fights  : " + maxFightMeans);
		pw.println("----------------------------------------------");
		if (this.bestInd != null) {
			pw.println("Best individual");
			pw.println(this.bestInd.toString());
			pw.println("----------------------------------------------");
		}
	}

}
