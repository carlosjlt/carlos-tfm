package Statistics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class GlobalStatistics {

	private String statisticsPath = "";
	private int iteration = 0;

	private static GlobalStatistics inst;

	public static GlobalStatistics GetInst() {
		if (inst == null)
			inst = new GlobalStatistics();

		return inst;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private GlobalStatistics() {
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void InitStatistics(String path) {
		iteration = 0;
		this.statisticsPath = path;
		File f = new File(path);
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new BufferedWriter(new FileWriter(f, false)));
			pw.println("Iteration\tMax Fitness\tMean Fitness\tMean Std Desviation\tStd Desv of Std Desviation\tMean Fight Result\tMax Fight Result\tMin Fight Result\tMax Rule Number\tMean Rule Number\tBest Rule Number");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null)
				pw.close();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void AddRoundStatistics(RoundStatisticsBase round) {
		File f = new File(statisticsPath);
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
			pw.println((++iteration) + "\t" + round.maxFitness + "\t"
					+ round.meanFitness + "\t"
					+ round.meanStdDesviation + "\t" 
					+ round.stdDesvStdDesviation + "\t" 
					+ round.meanFightMeans + "\t"
					+ round.maxFightMeans + "\t" 
					+ round.minFightMeans + "\t" 
					+ round.maxRuleNumber + "\t"
					+ round.meanRuleNumber + "\t" + 
					round.bestRuleNumber);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null)
				pw.close();
		}
	}

}
