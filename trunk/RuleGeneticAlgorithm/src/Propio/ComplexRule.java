package Propio;

import Utils.SortedVector;
import Variables.Variable;

public class ComplexRule implements Comparable<Object> {

	int numUsages;
	double meanValue;

	SortedVector conditions = new SortedVector();
	Variable action;
	int maxConditions;

	String sValue = "";

	// ------------------------------------------------
	// ------------------------------------------------
	public ComplexRule(Variable condicion, Variable action,
			boolean initRandmly, int _maxConditions) {

		numUsages = 0;

		if (initRandmly) {
			condicion.InitRandomly();
			action.InitRandomly();
		}

		this.maxConditions = _maxConditions;
		this.conditions.add(condicion);
		this.action = action;

		// La primera llamada a toString crear� el sValue. Hasta entonces no
		// hace falta
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void AddCondition(Variable condicion) {
		if (this.conditions.size() < maxConditions) {
			this.conditions.add(condicion);
			this.CreateString();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public boolean equals(Regla regla) {
		return this.toString().equalsIgnoreCase(regla.toString());
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private void CreateString() {
		sValue = "if ";
		for (Comparable<Object> con : conditions) {
			sValue += con.toString();
			sValue += " and ";
		}
		sValue = sValue.substring(0, sValue.length() - 4);
		sValue += " then " + action.toString();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public String toString() {

		if (sValue.isEmpty())
			this.CreateString();

		return this.sValue;
	}

	@Override
	public int compareTo(Object paramT) {
		return this.toString().compareTo(paramT.toString());
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public double GetMeanValue()
	{
		return meanValue;
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public int GetUsages()
	{
		return this.numUsages;
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public void AddValue(int value)
	{
		this.meanValue = ((this.meanValue*this.numUsages + value) / ++this.numUsages);
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public void Clear()
	{
		this.meanValue = 0.0;
		this.numUsages = 0;
	}

}
