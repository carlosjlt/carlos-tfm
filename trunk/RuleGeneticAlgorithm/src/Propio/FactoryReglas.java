package Propio;

import java.util.Vector;

import Actions.Skill;
import Utils.SingleRandom;
import Variables.Variable;


public class FactoryReglas {

	private static FactoryReglas instance;

	// ------------------------------------------------
	// ------------------------------------------------
	private FactoryReglas() {
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public static FactoryReglas GetInstance() {
		if (instance == null)
			instance = new FactoryReglas();

		return instance;
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public Regla GetReglaActions()
	{
		return new Regla(FactoryVariables.GetInstance().GetRandomVariable(),
				FactoryVariables.GetInstance().GetRandomAction(), true);
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public Regla GetReglaActions(String varaible, String actionName, boolean initRandomly) {
		if(Skill.IsValidSkill(actionName))
		{
			return new Regla(FactoryVariables.GetInstance().GetVariable(varaible),
					FactoryVariables.GetInstance().GetSkill(actionName), initRandomly);
		}
		else
		{
			return new Regla(FactoryVariables.GetInstance().GetVariable(varaible),
					FactoryVariables.GetInstance().GetMovement(actionName), initRandomly);
		}
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public ComplexRule GetComplexRuleActions(int maxConditions)
	{
		ComplexRule cr = new ComplexRule(FactoryVariables.GetInstance().GetRandomVariable(),
				FactoryVariables.GetInstance().GetRandomAction(), true, maxConditions);
		
		int numConditions = SingleRandom.Rand().nextInt(maxConditions) + 1;
		Vector<String> condUsed = new Vector<String>();
		for(int i =1; i < numConditions; ++i)
		{
			Variable cond = FactoryVariables.GetInstance().GetRandomVariable();
			if(condUsed.contains(cond.GetName()))
			{
				--i;
				continue;
			}
			condUsed.add(cond.GetName());
			cond.InitRandomly();
			cr.AddCondition(cond);
		}
		
		return cr;
	}

}
