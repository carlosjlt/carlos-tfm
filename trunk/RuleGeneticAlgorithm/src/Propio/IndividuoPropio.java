package Propio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import GeneticAlgorithm.IndividuoBase;
import Utils.SortedVector;

public class IndividuoPropio extends IndividuoBase implements Cloneable {

	// Quitar el public
	public SortedVector listReglas = new SortedVector();

	private static Random rnd = new Random();

	public static final int MIN_RULES = 3;
	public static final int MAX_CONDITIONS = FactoryVariables.enumVariables.values().length;

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoPropio() {
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoPropio(int numRules) {
		while (this.listReglas.size() < numRules) {
			this.listReglas.add(FactoryReglas.GetInstance()
					.GetComplexRuleActions(MAX_CONDITIONS));
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void AddRegla(ComplexRule r) {
		this.listReglas.add(r);
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void RemoveRegla(ComplexRule r) {
		this.listReglas.remove(r);
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoPropio Mutation() {
		IndividuoPropio clon = this.clone();

		clon.listReglas.add(FactoryReglas.GetInstance().GetComplexRuleActions(
				MAX_CONDITIONS));

		return clon;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoPropio Reduction() {
		IndividuoPropio clon = this.clone();

		int len = listReglas.size();
		if (len > MIN_RULES) {
			int index = Math.abs(rnd.nextInt()) % len;
			clon.RemoveRegla((ComplexRule) listReglas.elementAt(index));
		}

		return clon;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoPropio[] Cross(IndividuoBase indivBase2) {
		IndividuoPropio indiv2 =(IndividuoPropio) indivBase2;
		IndividuoPropio[] hijos = new IndividuoPropio[] {
				new IndividuoPropio(), new IndividuoPropio() };

		Vector<ComplexRule> differents = new Vector<ComplexRule>();
		Vector<ComplexRule> equals = new Vector<ComplexRule>();

		int i = 0;
		int j = 0;

		// Comparamos regla a regla
		while (i < this.listReglas.size() && j < indiv2.listReglas.size()) {
			int comparation = this.listReglas.elementAt(i).compareTo(
					indiv2.listReglas.elementAt(j));
			if (comparation > 0) // New row in reglas2
			{
				differents.add((ComplexRule) indiv2.listReglas.elementAt(j++));
			} else if (comparation < 0) // New row in reglas1
			{
				differents.add((ComplexRule) this.listReglas.elementAt(i++));
			} else // Same rules
			{
				equals.add((ComplexRule) this.listReglas.elementAt(i++));
				++j;
			}
		}

		// Metemos las que falten de las reglas 1
		while (i < this.listReglas.size()) {
			differents.add((ComplexRule) this.listReglas.elementAt(i++));
		}

		// Metemos las que falten de las reglas 2
		while (j < indiv2.listReglas.size()) {
			differents.add((ComplexRule) indiv2.listReglas.elementAt(j++));
		}

		// Los comunes se quedan
		for (ComplexRule r : equals) {
			hijos[0].AddRegla(r);
			hijos[1].AddRegla(r);
		}

		// Los diferentes se mezclan y se reparten
		Collections.shuffle(differents);
		for (int it = 0; it < differents.size(); ++it) {
			hijos[it % 2].AddRegla(differents.elementAt(it));
		}

		return hijos;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void SaveToFile(String fileName) {
		File file;
		file = new File("./data/aiData/Rules/" + fileName);
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(
					file)));
			for (Comparable<Object> r : listReglas) {
				pw.println(r.toString());
			}

			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public String toString() {
		String s = "";
		for (Comparable<Object> r : listReglas) {
			s += r.toString() + " \n";
		}
		return s;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoPropio clone() {
		IndividuoPropio clone = new IndividuoPropio();
		for (Comparable<Object> cr : this.listReglas) {
			clone.AddRegla((ComplexRule) cr);
		}
		return clone;
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public int getNumRules()
	{
		return this.listReglas.size();
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public String[] getRuleList()
	{
		String[] result = new String[this.getNumRules()];
		int i = -1;
		for(Comparable<Object> rule : this.listReglas)
		{
			result[++i] = rule.toString();
		}
		return result;
	}


}
