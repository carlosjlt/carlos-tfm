package Propio;

import Actions.Action;
import Actions.Skill;
import Actions.Move.HorizontalMove;
import Actions.Move.Move;
import Actions.Move.VerticalMove;
import Utils.SingleRandom;
import Variables.Distancia;
import Variables.Energy;
import Variables.OpponentAction;
import Variables.Position;
import Variables.Score;
import Variables.Variable;

public class FactoryVariables {
	
	private static FactoryVariables instance;
	private static String[] VARIABLES;
	private static String[] MOVES;
	// Los SKILLS est�n en el fichero Skill
	
	public enum enumVariables
	{
		DISTANCIA, 
		OWN_ENERGY, 
		OPP_ENERGY,
		OWN_POSITION,
		OPP_POSITION,
		OPP_ACTION,
		SCORE
	}
	
	public enum enumMoves
	{ 
		HorizontalMove, 
		VerticalMove
	}

	//------------------------------------------------
	//------------------------------------------------
	private FactoryVariables (){
		enumVariables[] states = enumVariables.values();
	    VARIABLES = new String[states.length];
	    
	    for (int i = 0; i < states.length; i++) {
	    	VARIABLES[i] = states[i].name();
	    }
	    
	    enumMoves[] moves = enumMoves.values();
	    MOVES = new String[moves.length];
	    
	    for (int i = 0; i < moves.length; i++) {
	    	MOVES[i] = moves[i].name();
	    }
	    
	}

	//------------------------------------------------
	//------------------------------------------------
	public static FactoryVariables GetInstance()
	{
		if(instance == null)
			instance = new FactoryVariables();
		
		return instance;
	}

	//------------------------------------------------
	//------------------------------------------------
	public Variable GetVariable(String varaible)
	{
		switch (enumVariables.valueOf(varaible))
		{
		case DISTANCIA:
			return new Distancia();
		case OWN_ENERGY:
			return new Energy(true);
		case OPP_ENERGY:
			return new Energy(false);
		case OWN_POSITION:
			return new Position(true);
		case OPP_POSITION:
			return new Position(false);
		case OPP_ACTION:
			return new OpponentAction();
		case SCORE:
			return new Score();

		default:
			break;
		}
		return null;
	}
	
	//------------------------------------------------
	//------------------------------------------------
	public Variable GetRandomVariable()
	{
		return GetVariable(VARIABLES[Math.abs(SingleRandom.Rand().nextInt()) % VARIABLES.length]);
	}

	//------------------------------------------------
	//------------------------------------------------
	public Action GetRandomAction()
	{
		if(SingleRandom.Rand().nextBoolean()) // Movement
			return GetMovement(MOVES[Math.abs(SingleRandom.Rand().nextInt()) % MOVES.length]);
		else
			return new Skill(Skill.SKILLS[Math.abs(SingleRandom.Rand().nextInt()) % Skill.SKILLS.length]);
	}

	//------------------------------------------------
	//------------------------------------------------
	public Skill GetSkill(String skillName)
	{
		if(Skill.IsValidSkill(skillName))
			return new Skill(skillName);
		
		return null;
	}
	
	//------------------------------------------------
	//------------------------------------------------
	public Move GetMovement(String movementType)
	{
		if(movementType.equalsIgnoreCase("HorizontalMove"))
			return new HorizontalMove();
		else if(movementType.equalsIgnoreCase("VerticalMove"))
			return new VerticalMove();
		
		return null;
	}

}
