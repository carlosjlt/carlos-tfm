package GPL;

import grammar.Derivation;
import grammar.Grammar;
import grammar.GrammarException;

public class Gramatica {

	public static final Gramatica Instance = new Gramatica();

	Grammar gr;

	private Gramatica() {
		try {
			gr = new Grammar(
					"../GPL-master/GPL-master/Grammars/FuzzyGAGrammar.gr");
		} catch (GrammarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int minimumDepth() {
		return gr.minimumDepth();
	}

	public Derivation getNewDerivation(int maxDepth) throws GrammarException {
		return new Derivation(gr, maxDepth);
	}

}
