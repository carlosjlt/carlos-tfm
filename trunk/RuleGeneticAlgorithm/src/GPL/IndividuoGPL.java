package GPL;

import grammar.Derivation;
import grammar.Element;
import grammar.GrammarException;
import grammar.NonTerminal;
import grammar.Terminal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import jdsl.core.api.Position;
import jdsl.core.api.Tree;
import jdsl.core.ref.PreOrderIterator;
import GeneticAlgorithm.IndividuoBase;

public class IndividuoGPL extends IndividuoBase {
	Derivation listReglas;

	private static int ADDITIONAL_DEPTH = 50; // Pueden ser reglas o condiciones
												// muy largas :/
	private static int MAX_DEPTH = Gramatica.Instance.minimumDepth()
			+ ADDITIONAL_DEPTH;

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoGPL() throws GrammarException {
		listReglas = Gramatica.Instance.getNewDerivation(MAX_DEPTH);
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoGPL(Derivation listaReglas) {
		this.listReglas = listaReglas;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoGPL Mutation() {
		try {
			return new IndividuoGPL(listReglas.mutate());
		} catch (GrammarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public IndividuoGPL[] Cross(IndividuoBase padreBase2) {
		IndividuoGPL[] hijos = null;
		try {
			IndividuoGPL padre2 = (IndividuoGPL) padreBase2;
			List<Derivation> derivacionesHijos;
			derivacionesHijos = this.listReglas.crossoverWX(padre2.listReglas);
			hijos = new IndividuoGPL[derivacionesHijos.size()];
			int i = -1;
			for (Derivation lista : derivacionesHijos) {
				hijos[++i] = new IndividuoGPL(lista);
			}
		} catch (GrammarException e) {
			e.printStackTrace();
		}

		return hijos;
	}

	@Override
	public String toString() {
		String result = "";
		for (String rule : this.getRuleList())
			result += rule + " \n";

		return result;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void SaveToFile(String fileName) throws IOException {
		File file;
		file = new File("./data/aiData/Rules/" + fileName);
		PrintWriter pw = new PrintWriter(new BufferedWriter(
				new FileWriter(file)));
		pw.println(listReglas.getWord());
		pw.close();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public String[] getRuleList() {
		return this.listReglas.getWord().split(" ; ");
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public int getNumRules()
	{
		return this.getRuleList().length;
	}
	

}
