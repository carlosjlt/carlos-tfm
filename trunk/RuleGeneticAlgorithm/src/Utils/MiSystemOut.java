package Utils;

import java.io.OutputStream;
import java.io.PrintStream;

public class MiSystemOut {
	
	public static final PrintStream out = System.out;
	public static final MiSystemOut Inst = new MiSystemOut();
	
	private MiSystemOut()
	{
		System.setOut(new PrintStream(new OutputStream() {
			    public void write(int b) {
			    }
			}));
	}
/*	
	public void println(String msg)
	{
		psOut.println(msg);
	}

	public void print(String msg)
	{
		psOut.print(msg);
	}
*/
}
