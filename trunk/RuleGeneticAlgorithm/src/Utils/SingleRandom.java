package Utils;

import java.util.Random;

public class SingleRandom {
	
	private static Random rnd;
	
	public static Random Rand()
	{
		if(rnd == null)
			rnd = new Random();
		
		return rnd;
	}
	
}
