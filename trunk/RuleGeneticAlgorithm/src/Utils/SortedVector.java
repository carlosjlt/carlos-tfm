package Utils;

import java.util.Iterator;
import java.util.Vector;

public class SortedVector implements Iterable<Comparable<Object>> {

	public Vector<Comparable<Object>> listObjects = new Vector<Comparable<Object>>();
	
	// ------------------------------------------------
	// ------------------------------------------------
	public void add(Comparable<Object> r) {
		int pos = 0;
		while(pos < listObjects.size() && listObjects.elementAt(pos).compareTo(r) < 0)
			++pos;
		
		if(pos == listObjects.size() || listObjects.elementAt(pos).compareTo(r) != 0) // Si es 0 ya existe.
			listObjects.insertElementAt(r, pos);
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void remove(Comparable<Object> r) {
		int pos = 0;
		while(pos < listObjects.size())
		{
			if(listObjects.elementAt(pos).compareTo(r) == 0)
				listObjects.removeElementAt(pos);
			
			++pos;
		}
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public int size()
	{
		return listObjects.size();
	}
	
	// ------------------------------------------------
	// ------------------------------------------------
	public Comparable<Object> elementAt(int index)
	{
		return listObjects.elementAt(index);
	}

	@Override
	public Iterator<Comparable<Object>> iterator() {
        return listObjects.iterator();
	}

}
