package Test;
import org.junit.Assert;
import org.junit.Test;

import Buttons.Button;
import Buttons.ButtonA;
import Buttons.ButtonB;
import Propio.IndividuoPropio;
import Propio.Regla;
import Variables.Distancia;


public class TestCruce {

	@Test
	public void test() {
		
		Distancia d = new Distancia();
		d.SetSelectedValue("FAR_LEFT");
		Distancia d2 = new Distancia();
		d2.SetSelectedValue("FAR_LEFT");

		Distancia d3 = new Distancia();
		d3.SetSelectedValue("FAR_RIGHT");

		Button b = new ButtonA();
		b.SetSelectedValue("PRESSED");

		Button b2 = new ButtonB();
		b2.SetSelectedValue("PRESSED");

		IndividuoPropio ind = new IndividuoPropio();
		ind.AddRegla(new Regla(d, b, false));
		ind.AddRegla(new Regla(d, b2, false));
		
		IndividuoPropio ind2 = new IndividuoPropio();
		ind2.AddRegla(new Regla(d2, b, false));
		ind2.AddRegla(new Regla(d3, b2, false));
		
		IndividuoPropio[] hijos = ind.Cross(ind2);
		
		System.out.println("Hijo 1");
		for(Comparable<Object> r : hijos[0].listReglas)
			System.out.println(r.toString());

		System.out.println("Hijo 2");
		for(Comparable<Object> r : hijos[1].listReglas)
			System.out.println(r.toString());

		Assert.assertTrue(hijos[0].listReglas.elementAt(0).toString().compareToIgnoreCase(hijos[1].listReglas.elementAt(0).toString()) == 0);
		Assert.assertTrue(hijos[0].listReglas.elementAt(1).toString().compareToIgnoreCase(hijos[1].listReglas.elementAt(1).toString()) != 0);
		
	}

}
