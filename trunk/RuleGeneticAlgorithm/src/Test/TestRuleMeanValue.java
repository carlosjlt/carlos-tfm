package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import Propio.ComplexRule;
import Propio.FactoryReglas;


public class TestRuleMeanValue {

	@Test
	public void test() {
		ComplexRule cr = FactoryReglas.GetInstance().GetComplexRuleActions(3);
		cr.AddValue(1000);
		cr.AddValue(900);
		cr.AddValue(800);
		assertEquals(900, cr.GetMeanValue(), 0.01);
		
		cr.AddValue(0);
		assertEquals(675, cr.GetMeanValue(), 0.01);
	}

}
