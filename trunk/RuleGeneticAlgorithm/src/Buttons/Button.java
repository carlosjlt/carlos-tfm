package Buttons;

import Variables.Variable;

public abstract class Button extends Variable
{
	
	public static final String[] VALUES = { "PRESSED", "RELEASED" };
	
	public Button()
	{
		_VALUES = VALUES;
	}	
}
