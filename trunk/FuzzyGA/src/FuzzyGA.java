import enumerate.Action;
import gameInterface.AIInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import structs.CharacterData;
import structs.FrameData;
import structs.GameData;
import structs.Key;
import FuzzyIA.RuleFilesManager;

import com.fuzzylite.Engine;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Rectangle;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

import commandcenter.CommandCenter;

public class FuzzyGA implements AIInterface {

	public static final String[] SKILLS = { "STAND_GUARD", "CROUCH_GUARD",
			"AIR_GUARD", "THROW_A", "THROW_B", "STAND_A", "STAND_B",
			"CROUCH_A", "CROUCH_B", "AIR_A", "AIR_B", "AIR_DA", "AIR_DB",
			"STAND_FA", "STAND_FB", "CROUCH_FA", "CROUCH_FB", "AIR_FA",
			"AIR_FB", "AIR_UA", "AIR_UB", "STAND_D_DF_FA", "STAND_D_DF_FB",
			"STAND_F_D_DFA", "STAND_F_D_DFB", "STAND_D_DB_BA", "STAND_D_DB_BB",
			"AIR_D_DF_FA", "AIR_D_DF_FB", "AIR_F_D_DFA", "AIR_F_D_DFB",
			"AIR_D_DB_BA", "AIR_D_DB_BB", "STAND_D_DF_FC" };

	Engine skillEngine;
	Engine move2ButtonsEngine = new Engine("MoveToButtons");

	Random rnd;
	Key inputKey;
	CommandCenter cc;
	FrameData frameData;
	String activeSkillName;

	boolean iAm1 = false;
	Vector<String> skillList = new Vector<String>();

	double maxX;
	double maxY;
	double centreX;

	String ruleSet;

	int llamadasAProcess = 0;

	public String getName() {
		return "FuzzyGA";
	}

	@Override
	public void close() {
		// System.out.println("llamadasAProcess -> " + llamadasAProcess);
		RuleFilesManager.GetInstance().setLlamadasAProcess(llamadasAProcess);
	}

	@Override
	public String getCharacter() {
		return CHARACTER_ZEN;
	}

	@Override
	public void getInformation(FrameData frameData) {
		this.frameData = frameData;
		cc.setFrameData(this.frameData, this.iAm1);
	}

	private void LoadRules() {
		if (!RuleFilesManager.GetInstance().IsInit())
			RuleFilesManager.GetInstance().Reset();

		File f = new File(RuleFilesManager.GetInstance().GetNextRuleFile(this.iAm1));
		ruleSet = f.getName();
		System.out.println(f.getAbsolutePath());

		RuleFilesManager.GetInstance().MatchThreadToRuleSet(f.getName(),
				Thread.currentThread().getName(), iAm1);

		try {
			RuleBlock ruleBlock = new RuleBlock();
			List<String> rules = Files.readAllLines(f.toPath());
			for (int i = 0; i < rules.size(); ++i) {
				// EL # es para comentar una regla
				if (rules.get(i).compareToIgnoreCase("") != 0
						&& !rules.get(i).startsWith("#")) {

					String[] ruleVector = rules.get(i).split(";");
					for (String rule : ruleVector)
						ruleBlock.addRule(Rule.parse(rule, skillEngine));
				}
			}
			skillEngine.addRuleBlock(ruleBlock);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addButtonOutput(String name, Engine engine) {
		OutputVariable button = new OutputVariable("BUTTON_" + name, 0.000,
				1.000);
		button.setDefaultValue(0.0);
		button.addTerm(new Rectangle("RELEASED", 0.000, 0.500));
		button.addTerm(new Rectangle("PRESSED", 0.500, 1.000));
		engine.addOutputVariable(button);
	}

	private void initMoveEngine() {

		// -- Input variables --

		InputVariable horizontal = new InputVariable("HorizontalMove", 0.000,
				1.000);
		horizontal.addTerm(new Trapezoid("BACK", 0.0000, 0.000, 0.250, 0.350));
		horizontal.addTerm(new Trapezoid("STAND", 0.35, 0.450, 0.55, 0.65));
		horizontal.addTerm(new Trapezoid("FORWARD", 0.55, 0.65, 1.0, 1.0));
		move2ButtonsEngine.addInputVariable(horizontal);

		InputVariable front = new InputVariable("FRONT", 0.000, 1.000);
		front.addTerm(new Rectangle("FALSE", 0.000, 0.500));
		front.addTerm(new Rectangle("TRUE", 0.500, 1.000));
		move2ButtonsEngine.addInputVariable(front);

		InputVariable vertical = new InputVariable("VerticalMove", 0.000, 1.000);
		vertical.addTerm(new Trapezoid("CROUCH", 0.0000, 0.000, 0.250, 0.350));
		vertical.addTerm(new Trapezoid("STAND", 0.35, 0.450, 0.55, 0.65));
		vertical.addTerm(new Trapezoid("JUMP", 0.55, 0.65, 1.0, 1.0));
		move2ButtonsEngine.addInputVariable(vertical);

		// -- Output variables --

		addButtonOutput("L", move2ButtonsEngine);
		addButtonOutput("R", move2ButtonsEngine);
		addButtonOutput("U", move2ButtonsEngine);
		addButtonOutput("D", move2ButtonsEngine);

		// -- Rules --

		RuleBlock ruleBlock = new RuleBlock();
		ruleBlock
				.addRule(Rule
						.parse("if (HorizontalMove is FORWARD) and (FRONT is TRUE) then BUTTON_R is PRESSED",
								move2ButtonsEngine));
		ruleBlock
				.addRule(Rule
						.parse("if (HorizontalMove is BACK) and (FRONT is TRUE) then BUTTON_L is PRESSED",
								move2ButtonsEngine));
		ruleBlock
				.addRule(Rule
						.parse("if (HorizontalMove is FORWARD) and (FRONT is FALSE) then BUTTON_L is PRESSED",
								move2ButtonsEngine));
		ruleBlock
				.addRule(Rule
						.parse("if (HorizontalMove is BACK) and (FRONT is FALSE) then BUTTON_R is PRESSED",
								move2ButtonsEngine));

		ruleBlock.addRule(Rule.parse(
				"if VerticalMove is JUMP then BUTTON_U is PRESSED",
				move2ButtonsEngine));
		ruleBlock.addRule(Rule.parse(
				"if VerticalMove is CROUCH then BUTTON_D is PRESSED",
				move2ButtonsEngine));

		move2ButtonsEngine.addRuleBlock(ruleBlock);

		move2ButtonsEngine.configure("AlgebraicProduct", "NormalizedSum",
				"AlgebraicProduct", "AlgebraicSum", "Centroid");

	}

	// -----------------------------------------------------------------------------
	// -----------------------------------------------------------------------------
	private void SkillVariableGeneration(String name, Engine engine) {
		OutputVariable outVar = new OutputVariable(name, 0.000, 1.000);
		outVar.setDefaultValue(0.0);
		outVar.addTerm(new Trapezoid("DONT_DO", 0.0, 0.0, 0.4, 0.5));
		outVar.addTerm(new Trapezoid("TRY", 0.4, 0.5, 0.6, 0.7));
		outVar.addTerm(new Trapezoid("DO_IT", 0.6, 0.7, 1.0, 1.0));
		engine.addOutputVariable(outVar);

		skillList.add(name);
	}

	// -----------------------------------------------------------------------------
	// -----------------------------------------------------------------------------
	private void initRuleEngine() {
		skillEngine = new Engine("fuzzyEngine");

		// Input variables

		InputVariable distancia = new InputVariable("DISTANCIA", 0.000, 1.000);
		distancia.addTerm(new Trapezoid("VERY_CLOSE", 0.0, 0.0,
				20.0 / this.maxX, 30.0 / this.maxX));
		distancia.addTerm(new Trapezoid("CLOSE", 20.0 / this.maxX,
				30.0 / this.maxX, 0.4, 0.5));
		distancia.addTerm(new Trapezoid("FAR", 0.4, 0.5, 0.7, 0.8));
		distancia.addTerm(new Trapezoid("VERY_FAR", 0.7, 0.8, 1.0, 1.0));
		skillEngine.addInputVariable(distancia);

		InputVariable energy = new InputVariable("OWN_ENERGY");
		energy.addTerm(new Rectangle("NONE", 0.0, 30.0));
		energy.addTerm(new Trapezoid("LOW", 30.0, 30.0, 40.0, 50.0));
		energy.addTerm(new Trapezoid("GOOD", 40.0, 50.0, 300.0, 300.0));
		energy.addTerm(new Rectangle("FULL", 300.0, 5000.0));
		skillEngine.addInputVariable(energy);

		InputVariable oppEnergy = new InputVariable("OPP_ENERGY");
		oppEnergy.addTerm(new Rectangle("NONE", 0.0, 30.0));
		oppEnergy.addTerm(new Trapezoid("LOW", 30.0, 30.0, 40.0, 50.0));
		oppEnergy.addTerm(new Trapezoid("GOOD", 40.0, 50.0, 300.0, 300.0));
		oppEnergy.addTerm(new Rectangle("FULL", 300.0, 5000.0));
		skillEngine.addInputVariable(oppEnergy);

		InputVariable positionOnScene = new InputVariable("OWN_POSITION",
				-120.0, centreX);
		positionOnScene.addTerm(new Trapezoid("SIDE", -120.0, -120.0,
				this.centreX * 0.10, this.centreX * 0.20));
		positionOnScene.addTerm(new Trapezoid("CENTER", centreX * 0.10,
				centreX * 0.20, centreX, centreX));
		skillEngine.addInputVariable(positionOnScene);

		InputVariable oppPositionOnScene = new InputVariable("OPP_POSITION",
				-120.0, centreX);
		oppPositionOnScene.addTerm(new Trapezoid("SIDE", -120.0, -120.0,
				centreX * 0.10, centreX * 0.20));
		oppPositionOnScene.addTerm(new Trapezoid("CENTER", centreX * 0.10,
				centreX * 0.20, centreX, centreX));
		skillEngine.addInputVariable(oppPositionOnScene);

		InputVariable oppAction = new InputVariable("OPP_ACTION");
		for (Action val : Action.values()) {
			oppAction.addTerm(new Rectangle(val.name(), val.ordinal() - 0.5,
					val.ordinal() + 0.5));
		}
		skillEngine.addInputVariable(oppAction);

		InputVariable gameScore = new InputVariable("SCORE", 0.0, 1000.0);
		gameScore.addTerm(new Trapezoid("LOSE", 0.0, 0.0, 450.0, 500.0));
		gameScore.addTerm(new Triangle("DRAW", 450.0, 500.0, 550.0));
		gameScore.addTerm(new Trapezoid("WIN", 500.0, 550.0, 1000.0, 1000.0));
		skillEngine.addInputVariable(gameScore);

		// Los individuos no generan reglas para esta variable
		InputVariable oppButtonA = new InputVariable();
		oppButtonA.setName("OppButtonA");
		oppButtonA.setRange(0.000, 1.000);
		oppButtonA.addTerm(new Rectangle("RELEASED", 0.000, 0.500));
		oppButtonA.addTerm(new Rectangle("PRESSED", 0.500, 1.000));
		skillEngine.addInputVariable(oppButtonA);
		// ------------------------------------------------------

		// Output variables

		// -- Skills
		for (String skillName : SKILLS)
			SkillVariableGeneration(skillName, skillEngine);

		// -- Movement

		OutputVariable horizontal = new OutputVariable("HorizontalMove", 0.000,
				1.000);
		horizontal.setDefaultValue(0.5);
		horizontal.addTerm(new Trapezoid("BACK", 0.0000, 0.000, 0.250, 0.350));
		horizontal.addTerm(new Trapezoid("STAND", 0.35, 0.450, 0.55, 0.65));
		horizontal.addTerm(new Trapezoid("FORWARD", 0.55, 0.65, 1.0, 1.0));
		skillEngine.addOutputVariable(horizontal);

		OutputVariable vertical = new OutputVariable("VerticalMove", 0.000,
				1.000);
		vertical.setDefaultValue(0.5);
		vertical.addTerm(new Trapezoid("CROUCH", 0.0000, 0.000, 0.250, 0.350));
		vertical.addTerm(new Trapezoid("STAND", 0.35, 0.450, 0.55, 0.65));
		vertical.addTerm(new Trapezoid("JUMP", 0.55, 0.65, 1.0, 1.0));
		skillEngine.addOutputVariable(vertical);

		LoadRules();

		skillEngine.configure("AlgebraicProduct", "NormalizedSum",
				"AlgebraicProduct", "AlgebraicSum", "Centroid");

	}

	@Override
	public int initialize(GameData gameData, boolean isPlayer1) {

		/*
		 * FuzzyLite.setDebug(true); FuzzyLite.setLogging(true); try { Handler
		 * fh = new FileHandler("./wombat.log");
		 * FuzzyLite.logger().addHandler(fh); } catch (SecurityException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); } catch
		 * (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		this.rnd = new Random();
		this.inputKey = new Key();
		this.iAm1 = isPlayer1;
		this.maxX = gameData.getStageXMax();
		this.maxY = gameData.getStageYMax();
		this.centreX = this.maxX / 2.0;

		this.initMoveEngine();
		this.initRuleEngine();

		cc = new CommandCenter();

		return 0;
	}

	@Override
	public Key input() {
		return inputKey;
	}

	@Override
	public String toString() {
		return "FuzzyGA_" + this.ruleSet;
	}

	@Override
	public void processing() {
		// System.out.println("------------------------------- Process Fuzzy ---------------------------------------------------");
		llamadasAProcess++;
		// System.out.println("Remaining time: " + frameData.getRemainingTime()
		// );
		if (!frameData.emptyFlag && frameData.getRemainingTime() > 0) {
			CharacterData ownData = frameData.getMyCharacter(iAm1);
			CharacterData oppData = frameData.getOpponentCharacter(iAm1);

			double score = 500;
			if ((ownData.getHp() != 0) || (oppData.getHp() != 0))
				score = (1000 * oppData.getHp() / (ownData.getHp() + oppData
						.getHp()));
			/*
			 * System.out.println("Fuzzy Score: " + score + " - HP: " +
			 * ownData.getHp() + " - OPP_HP: " + oppData.getHp());
			 * System.out.println("Positions: own: " + ownData.getX() +
			 * " - opp: " + oppData.getX());
			 * System.out.println("Actions:   own: " +
			 * ownData.getAction().toString() + " - opp: " +
			 * oppData.getAction().toString());
			 * System.out.println("Speeds:    own: " + ownData.getSpeedX() +
			 * " - opp: " + oppData.getSpeedX());
			 * System.out.println("Area:      own: L " + ownData.getLeft() +
			 * " R: " + ownData.getRight());
			 * System.out.println("Diff Area Pos : " + (ownData.getLeft() -
			 * ownData.getX()));
			 */

			if (cc.getskillFlag()) {
				inputKey = cc.getSkillKey();
				// System.out.println("Skill -> " +
				// cc.getMyCharacter().getAction().name() + " " +
				// frameData.getMyCharacter(iAm1).getRemainingFrame());
			} else {

				// Set input variables
				skillEngine.setInputValue("DISTANCIA",
						Math.abs(ownData.getX() - oppData.getX()) / this.maxX);
				skillEngine.setInputValue("OWN_ENERGY", ownData.getEnergy());
				skillEngine.setInputValue("OPP_ENERGY", oppData.getEnergy());

				if (ownData.getX() > centreX)
					skillEngine.setInputValue("OWN_POSITION", ownData.getX()
							- centreX);
				else
					skillEngine.setInputValue("OWN_POSITION", ownData.getX());

				if (oppData.getX() > centreX)
					skillEngine.setInputValue("OPP_POSITION", oppData.getX()
							- centreX);
				else
					skillEngine.setInputValue("OPP_POSITION", oppData.getX());

				skillEngine.setInputValue("OPP_ACTION", oppData.getAction()
						.ordinal());

				skillEngine.setInputValue("OppButtonA",
						frameData.keyData.getOpponentKey(iAm1).A ? 1.0 : 0.0);

				skillEngine.setInputValue("SCORE", score);

				/*
				 * System.out.println("Fuzzy Score: " + score + " - HP: " +
				 * ownData.getHp() + " - OPP_HP: " + oppData.getHp());
				 * System.out.println("Positions: own: " + ownData.getX() +
				 * " - opp: " + oppData.getX());
				 * System.out.println("Actions:   own: " +
				 * ownData.getAction().toString() + " - opp: " +
				 * oppData.getAction().toString());
				 * System.out.println("Speeds:    own: " + ownData.getSpeedX() +
				 * " - opp: " + oppData.getSpeedX());
				 */

				skillEngine.process();

				// If skillEngine dice activarSkill -> Activar skill
				activeSkillName = "";
				List<OutputVariable> outputs = skillEngine.getOutputVariables();
				OutputVariable best = skillEngine.getOutputVariable("HorizontalMove");
				for (OutputVariable var : outputs) {
					if (var.defuzzify() > best.defuzzify())
						best = var;
				}
				
				for (String skillName : this.skillList) {
					if(skillName.compareToIgnoreCase(best.getName()) == 0)
					{
						activeSkillName = skillName;
						break;
					}
				}
				if (!activeSkillName.isEmpty()) {
					inputKey.empty();
					cc.skillCancel();
					cc.commandCall(activeSkillName);
				} else {
					move2ButtonsEngine.getInputVariable("FRONT").setInputValue(
							frameData.getMyCharacter(iAm1).isFront() ? 0.9
									: 0.1);

					move2ButtonsEngine.getInputVariable("HorizontalMove")
							.setInputValue(
									skillEngine
											.getOutputValue("HorizontalMove"));

					move2ButtonsEngine.getInputVariable("VerticalMove")
							.setInputValue(
									skillEngine.getOutputValue("VerticalMove"));

					move2ButtonsEngine.process();

					inputKey.L = (move2ButtonsEngine.getOutputValue("BUTTON_L") > 0.5) ? true
							: false;
					inputKey.R = (move2ButtonsEngine.getOutputValue("BUTTON_R") > 0.5) ? true
							: false;
					inputKey.U = (move2ButtonsEngine.getOutputValue("BUTTON_U") > 0.5) ? true
							: false;
					inputKey.D = (move2ButtonsEngine.getOutputValue("BUTTON_D") > 0.5) ? true
							: false;
				}
			}
		}
		// System.out.println("-------------------------------------------------------------------------------------------");
	}
}
