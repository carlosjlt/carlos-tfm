package FuzzyIA;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Vector;

public class RuleFilesManager {

	public static final String END_INDIV = "_indiv.txt";
	public static final String END_SPARRING = "_sparr.txt";

	Vector<String> aiFiles = new Vector<String>();
	Vector<String> aiSparringFiles = new Vector<String>();
	int currentFile;
	int currentSparringFile;
	boolean isInit = false;
	boolean saveProcessCalls = true;

	public static final String LLAMADAS_PROCESS_CALLS = "./data/aiData/ProcessCalls.txt";
	public static final String THREADS_TO_RULESET = "./data/aiData/ThreadsToRuleSet.txt";

	private static RuleFilesManager inst;

	// ------------------------------------------------
	// ------------------------------------------------
	public static RuleFilesManager GetInstance() {
		if (inst == null)
			inst = new RuleFilesManager();

		return inst;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void SetInitFile(int initFile, boolean getIndiv) {
		if(getIndiv)
			this.currentFile = initFile;
		else
			this.currentSparringFile = initFile;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public static void InitInstance() {
		if (inst == null)
			inst = new RuleFilesManager();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	private RuleFilesManager() {
		// Reset();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public boolean IsInit() {
		return isInit;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void setSaveProcessCalls(boolean _saveProcessCalls) {
		this.saveProcessCalls = _saveProcessCalls;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public synchronized String GetNextRuleFile(boolean getIndiv) {
		if (getIndiv) {
			if (currentFile == aiFiles.size() - 1) {
				// Collections.shuffle(aiFiles); // Poner cuando se enfrenten
				// entre ellos
				currentFile = -1;
			}
			// System.out.println("File -> " + (currentFile +1) %
			// aiFiles.size());
			return aiFiles.elementAt(++currentFile % aiFiles.size());
		}
		else
		{
			if (currentSparringFile == aiSparringFiles.size() - 1) {
				// Collections.shuffle(aiFiles); // Poner cuando se enfrenten
				// entre ellos
				currentSparringFile = -1;
			}
			// System.out.println("File -> " + (currentFile +1) %
			// aiFiles.size());
			return aiSparringFiles.elementAt(++currentSparringFile % aiSparringFiles.size());
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public void ResetThreadsToRuleSetFile() {
		File f = new File(THREADS_TO_RULESET);
		f.delete();

		f = new File(LLAMADAS_PROCESS_CALLS);
		f.delete();
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public synchronized void Reset() {
		aiFiles.clear();
		//ResetThreadsToRuleSetFile();

		String path = "./data/aiData/Rules";
		File dir = new File(path);
		File[] files = dir.listFiles();

		for (int i = 0; i < files.length; i++) {
			String buffer = files[i].toString();
			if (buffer.endsWith(END_INDIV)) {
				aiFiles.add(String.valueOf(buffer));
			} else if (buffer.endsWith(END_SPARRING)) {
				aiSparringFiles.add(String.valueOf(buffer));
			}
		}

		currentFile = -1; // Para que el primero comience en 0
		currentSparringFile = -1;
		isInit = true;
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public synchronized void MatchThreadToRuleSet(String ruleSet,
			String ThreadName, boolean player1) {
		try {
			File f = new File(THREADS_TO_RULESET);
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(
					f, true)));
			pw.println(ruleSet + " " + ThreadName + " " + player1);
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ------------------------------------------------
	// ------------------------------------------------
	public synchronized void setLlamadasAProcess(int llamadasAProcess) {
		if (this.saveProcessCalls) {
			try {
				File f = new File(LLAMADAS_PROCESS_CALLS);
				PrintWriter pw = new PrintWriter(new BufferedWriter(
						new FileWriter(f, true)));
				pw.println(llamadasAProcess);
				pw.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
