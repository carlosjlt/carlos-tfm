import structs.FrameData;
import structs.GameData;
import structs.Key;
import gameInterface.AIInterface;


public class NoAction implements AIInterface {

	Key inputKey;

	@Override
	public void close() {
	}

	@Override
	public String getCharacter() {
		return CHARACTER_LUD;
	}

	@Override
	public void getInformation(FrameData arg0) {
	}

	@Override
	public int initialize(GameData arg0, boolean arg1) {
		inputKey = new Key();
		return 0;
	}

	@Override
	public Key input() {
		return inputKey;
	}

	@Override
	public void processing() {
		// every key is set to false.
		inputKey.A = false;
		inputKey.B = false;
		inputKey.C = false;
		inputKey.U = false;
		inputKey.D = false;
		inputKey.L = false;
		inputKey.R = false;
		
	}

}
