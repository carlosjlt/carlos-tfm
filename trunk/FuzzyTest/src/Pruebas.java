import com.fuzzylite.Engine;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Rectangle;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;


public class Pruebas {

	public static void main(String[] args)
	{
		Engine mtb = new Engine("MoveToButtons");
		Engine rtm = new Engine("RulesToMove");
		
		// ---- Move to buttons ---
		
		InputVariable horizontal = new InputVariable("HorizontalMove", 0.000, 1.000);
		horizontal.addTerm(new Trapezoid("BACK", 0.0000, 0.000, 0250, 0.350));
		horizontal.addTerm(new Trapezoid("STAND", 0.35, 0.450, 0.55, 0.65));
		horizontal.addTerm(new Trapezoid("FORWARD", 0.55, 0.65, 1.0, 1.0));
		mtb.addInputVariable(horizontal);

		InputVariable front = new InputVariable("FRONT", 0.000, 1.000);
		front.addTerm(new Rectangle("FALSE", 0.000, 0.500));
		front.addTerm(new Rectangle("TRUE", 0.500, 1.000));
		mtb.addInputVariable(front);

		OutputVariable buttonL = new OutputVariable();
		buttonL.setName("BUTTON_L");
		buttonL.setRange(0.000, 1.000);
		buttonL.addTerm(new Rectangle("RELEASED", 0.000, 0.500));
		buttonL.addTerm(new Rectangle("PRESSED", 0.500, 1.000));
		mtb.addOutputVariable(buttonL);

		OutputVariable buttonR = new OutputVariable();
		buttonR.setName("BUTTON_R");
		buttonR.setRange(0.000, 1.000);
		buttonR.addTerm(new Rectangle("RELEASED", 0.000, 0.500));
		buttonR.addTerm(new Rectangle("PRESSED", 0.500, 1.000));
		mtb.addOutputVariable(buttonR);
		
		RuleBlock ruleBlock = new RuleBlock();
		ruleBlock.addRule(Rule.parse("if (HorizontalMove is FORWARD) and (FRONT is TRUE) then BUTTON_R is PRESSED", mtb));
		ruleBlock.addRule(Rule.parse("if (HorizontalMove is BACK) and (FRONT is TRUE) then BUTTON_L is PRESSED", mtb));
		ruleBlock.addRule(Rule.parse("if (HorizontalMove is FORWARD) and (FRONT is FALSE) then BUTTON_L is PRESSED", mtb));
		ruleBlock.addRule(Rule.parse("if (HorizontalMove is BACK) and (FRONT is FALSE) then BUTTON_R is PRESSED", mtb));
		mtb.addRuleBlock(ruleBlock);

		mtb.configure("AlgebraicProduct", "NormalizedSum", "AlgebraicProduct", "AlgebraicSum", "Centroid");

		// --- Rules to move ---
		
		InputVariable mode = new InputVariable("Move", 0.000, 1.000);
		mode.addTerm(new Trapezoid("BACK", 0.0000, 0.000, 0250, 0.350));
		mode.addTerm(new Trapezoid("STAND", 0.35, 0.450, 0.55, 0.65));
		mode.addTerm(new Trapezoid("FORWARD", 0.55, 0.65, 1.0, 1.0));
		rtm.addInputVariable(mode);

		OutputVariable horizontalO = new OutputVariable("HorizontalMove", 0.000, 1.000);
		horizontalO.addTerm(new Trapezoid("BACK", 0.0000, 0.000, 0250, 0.350));
		horizontalO.addTerm(new Trapezoid("STAND", 0.35, 0.450, 0.55, 0.65));
		horizontalO.addTerm(new Trapezoid("FORWARD", 0.55, 0.65, 1.0, 1.0));
		rtm.addOutputVariable(horizontalO);
		

		RuleBlock ruleBlock2 = new RuleBlock();
		ruleBlock2.addRule(Rule.parse("if Move is BACK then HorizontalMove is BACK", rtm));
		ruleBlock2.addRule(Rule.parse("if Move is STAND then HorizontalMove is STAND", rtm));
		ruleBlock2.addRule(Rule.parse("if Move is FORWARD then HorizontalMove is FORWARD", rtm));
		rtm.addRuleBlock(ruleBlock2);

		rtm.configure("AlgebraicProduct", "NormalizedSum", "AlgebraicProduct", "AlgebraicSum", "Centroid");


		// ---------------------
		
		mode.setInputValue(0.2);
		
		rtm.process();
		
		horizontal.setInputValue(horizontalO.defuzzify());
		front.setInputValue(1.0);
		
		mtb.process();

		System.out.println("Button_R = " + (buttonR.defuzzify() > 0.5 ? "P" : ""));
		System.out.println("Button_L = " + (buttonL.defuzzify() > 0.5 ? "P" : ""));
	/*
		System.out.println("Button_R = " + buttonR.defuzzify());
		System.out.println("Button_L = " + buttonL.defuzzify());
	 */
		
	}
}
